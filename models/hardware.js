var memfs = require('memfs');
const fp = require('path');
const fs = require('fs-extra');
const Volume = memfs.Volume;
const crypto = require('crypto');
const archiver = require('archiver');
const bcrypt = require('bcryptjs');

class CPU {
    constructor(speed, cores){
        this.speed = speed;
        this.cores = cores;
        this.slots = cores * 5;
    }
    toJSON(){
        return {
            speed: this.speed,
            cores: this.cores
        }
    }
}

class NetworkInterface {
    constructor(save){
        console.log(save);
        this.address = save.address;
        this.out = save.out;
        this.in = save.in;
        this.maxbuf = save.buffer;
        this.gateway = save.gateway;
        this.proxy = save.proxy;
        this.services = new Map();
        var serv = save.services || [];
        serv.forEach(service=>{
            this.services.set(service.port, service);
        });
    }
    toJSON(){
        return {
            buffer: this.maxbuf,
            gateway: this.gateway,
            proxy: this.proxy,
            address: this.address,
            out: this.out,
            in: this.in
        }
    }
    send(dest, port, payload){
        if(this.address == '') return false;
        if(this.out.length != this.maxbuf)
        {
            let packet = {
                source: this.address,
                dest: dest,
                port: port,
                payload: payload
            };
            this.out.push(packet);
            return true;
        }
        else return false;
    }
    receive(){
        if(this.address == '') return null;
        if(this.in.length > 0)
        {
            let res = this.in.slice();
            this.in = [];
            return res;
        }
        else return null;
    }
    read(){
        let buf = this.out.slice();
        this.out = [];
        return buf;
    }
    assign(ip){
        this.address = ip;
    }
}

class HDD {
    constructor(save){
        const self = this;
        this.size = save.size;
        this.vol = Volume.fromJSON(save.hdd, '/');
        this.perm = Map.fromJSON(save.perm);
    }
    toJSON(){
        return {
            size: this.size,
            hdd: this.vol.toJSON(),
            perm: this.perm.toJSON()
        }
    }
    remove_data(npath, recursive){
        const self = this;
        if(recursive === true)
        {
            function rimraf(dir_path) {
                if (self.vol.existsSync(dir_path)) {
                    self.vol.readdirSync(dir_path).forEach(function(entry) {
                        var entry_path = fp.join(dir_path, entry).replace(/\\/g,'\/');
                        if (self.vol.lstatSync(entry_path).isDirectory()) {
                            rimraf(entry_path);
                        } else {
                            self.vol.unlinkSync(entry_path);
                            if(self.perm.get(entry_path)) self.perm.delete(entry_path);
                        }
                    });
                    self.vol.rmdirSync(dir_path);
                    if(self.perm.get(dir_path)) self.perm.delete(dir_path);
                }
            }
            try {
                rimraf(npath);
                return {ok:true};
            } catch(e){
                return {ok:false,msg:e.message};
            }
        }
        else
        {
            if(self.vol.lstatSync(npath).isDirectory())
            {
                try{
                    self.vol.rmdirSync(npath);
                    if(self.perm.get(npath)) self.perm.delete(npath);
                    return {ok:true};
                }catch(e){
                    return {ok:false, msg:e.message};
                }
            }
            else
            {
                try{
                    self.vol.unlinkSync(npath);
                    if(self.perm.get(npath)) self.perm.delete(npath);
                    return {ok:true};
                }catch(e){
                    return {ok:false, msg:e.message};
                }
            }
        }
    }
    toZip(context){
        const self = this;
        var promise = new Promise((resolve, reject)=>{
            var home = context.HOME;
            var sdir = 'temp/save-'+context.caller+'-'+crypto.randomBytes(8).toString('hex');
            var savedir = process.cwd()+'/'+sdir;
            fs.ensureDirSync(savedir);
            var drive = self.vol.toJSON();
            var directories = [];
            var files = [];
            Object.keys(drive).forEach(path=>{
                if(!path.includes(home)) return;
                if(drive[path] == null) directories.push(path);
                else files.push({path: path, data: drive[path]});
            });
            directories.forEach(dir=>{
                fs.ensureDirSync(savedir+dir);
            });
            files.forEach(file=>{
                if(file.path == '/etc/passwd')
                {
                    var obj = JSON.parse(file.data);
                    Object.values(obj).forEach(val=>{
                        val.password = 'X';
                    });
                    file.data = JSON.stringify(obj);
                }
                fs.ensureFileSync(savedir+file.path);
                fs.writeFileSync(savedir+file.path, file.data);
            });
            var archive = archiver('zip', {
                zlib: {level: 7}
            });
            var output = fs.createWriteStream(savedir+'.zip');
            output.on('close', ()=>{
                fs.removeSync(savedir);
                resolve(sdir+'.zip');
            });
            archive.pipe(output);
            archive.directory(savedir, false);
            archive.finalize();
        });
        return promise;
    }
    check(data){
        let total = Buffer.from(Object.values(this.vol.toJSON()).filter(el=>{ return el != null }).join('')).length;
        total += Buffer.from(data).length;
        return (total <= this.size);
    }
    hasPermission(context, path, op){
        try {
            path = path.replace(/\\/g, '\/');
            console.log(op,path);
            if(typeof context == 'string')
                context = JSON.parse(context);
            let perm = this.perm.get(path);
            if(!perm) return true;
            let owner = perm.owner.split(":")[0];
            let group = perm.owner.split(':')[1];
            let access = 2;
            if(context.caller == owner || context.caller == 'root') access = 0;
            else if(context.groups.split(';').find(el=>{
                return el == group;
            })) access = 1;
            console.log("Operation:",op,'| Access:',access);
            let operation = perm[op];
            console.log(operation);
            if(!operation) return false;
            console.log(operation[access]);
            return operation[access];
        } catch(e){
            return false;
        }
    }
    getUsers(){
        var passwd = this.vol.readFileSync('/etc/passwd', 'utf8');
        passwd = JSON.parse(passwd);
        return passwd;
    }
    setUsers(passwd){
        try {
            this.vol.writeFileSync('/etc/passwd', JSON.stringify(passwd));
            return {ok:true}
        } catch(e){
            return {ok:false}
        }
    }
    login_user(username, ip){
        var passwd = this.getUsers();
        if(!passwd[username]) return {ok:false};
        passwd[username].last_login = Date.now();
        passwd[username].last_ip = ip;
        this.setUsers(passwd);
    }
    set_sudo(username, val){
        var passwd = this.getUsers();
        if(typeof val != 'boolean') return {ok:false};
        if(!passwd[username]) return {ok:false};
        passwd[username].sudo = val;
        this.setUsers(passwd);
        return {ok:true};
    }
    set_ssh(username, val){
        var passwd = this.getUsers();
        if(typeof val != 'boolean') return {ok:false};
        if(!passwd[username]) return {ok:false};
        passwd[username].can_ssh = val;
        this.setUsers(passwd);
        return {ok:true};
    }
    add_user(username, pass){
        var passwd = this.getUsers();
        var salt = bcrypt.genSaltSync(13);
        var hash = bcrypt.hashSync(pass, salt);
        if(passwd[username]) return "`DUser already exists.`";
        var obj = {
            password: hash,
            groups: "",
            sudo: false,
            home: `/home/${username}`,
            expiry: -1,
            last_login: 0,
            last_ip: '',
            can_ssh: true,
            logged_in: false
        };
        this.vol.mkdirpSync(`/home/${username}`);
        passwd[username] = obj;
        return this.setUsers(passwd);
    }
    can_sudo(username){
        var passwd = this.getUsers();
        if(!passwd[username]) return {ok:false};
        if(passwd[username].sudo === true) return {ok: true};
        else return {ok:false};
    }
    check_pass(username, pass){
        var passwd = this.getUsers();
        if(!passwd[username]) return {ok:false};
        if(bcrypt.compareSync(pass, passwd[username].password)) return {ok:true};
        else return {ok:false};
    }
    set_password(username, pass){
        var passwd = this.getUsers();
        if(!passwd[username]) return false;
        var salt = bcrypt.genSaltSync(13);
        var hash = bcrypt.hashSync(pass, salt);
        passwd[username].password = hash;
        passwd[username].expiry = -1;
        return this.setUsers(passwd);
    }
    expire_password(username){
        var passwd = this.getUsers();
        if(!passwd[username]) return false;
        passwd[username].expiry = 0;
        return this.setUsers(passwd);
    }
    auth_user(username, pass){
        var passwd = this.getUsers();
        if(!passwd[username]) return {ok:false};
        if(bcrypt.compareSync(pass, passwd[username].password) && passwd[username].expiry != -1 && passwd[username].expiry <= Date.now()) return {ok:false, reset:true, home: passwd[username].home, groups: passwd[username].groups, last_login: passwd[username].last_login, last_ip: passwd[username].last_ip};
        else if(bcrypt.compareSync(pass, passwd[username].password)) return {ok:true, home: passwd[username].home, groups: passwd[username].groups, last_login: passwd[username].last_login, last_ip: passwd[username].last_ip};
        else return {ok:false};
    }
    user_exists(username){
        var passwd = this.getUsers();
        if(!passwd[username]) return {ok:false};
        else return {ok:true}
    }
    usage(){
        let storage = Object.values(this.vol.toJSON());
        let files = storage.filter(el=>{
            return el != null;
        });
        let dirs = storage.filter(el=>{
            return el == null;
        });
        let total = Buffer.from(storage.join('')).length;
        return JSON.stringify({bytes: total, limit: this.size, percent: total/this.size, files: files.length, directories: dirs.length});
    }
    write_file(context, path, data){
        path = path.replace(/\\/g, '\/');
        if(!this.check(data)) return false;
        let parent = path.split('/');
        parent.pop();
        parent = parent.join('/');
        if(!this.hasPermission(context, path, 'write') && !this.hasPermission(context, parent, 'write')) return {ok:false, msg:"`DError`: Access Denied"};
        try {
            if(this.vol.existsSync(path))
            {
                this.vol.writeFileSync(path, data);
                return {ok:true};
            }
            else
            {
                if(!this.perm.get(path))
                    this.perm.set(path, {owner:`${context.caller}:root`, write:[true, true, false], read:[true, true, true], exec:[false, false, false]});
                this.vol.writeFileSync(path, data);
                return {ok:true};
            }
        } catch(e){
            return {ok:false, msg:"`DError`: "+e.message};
        }
    }
    append_file(context, path, data){
        path = path.replace(/\\/g, '\/');
        if(!this.check(data)) return false;
        let parent = path.split('/');
        parent.pop();
        parent = parent.join('/');
        if(!this.hasPermission(context, path, 'write') && !this.hasPermission(context, parent, 'write')) return {ok:false, msg:"`DError`: Access Denied"};
        try {
            if(this.vol.existsSync(path))
            {
                this.vol.appendFileSync(path, data);
                return {ok:true};
            }
            else
            {
                this.vol.writeFileSync(path, '');
                if(!this.perm.get(path))
                    this.perm.set(path, {owner:`${context.caller}:root`, write:[true, true, false], read:[true, true, true], exec:[false, false, false]});
                this.vol.appendFileSync(path, data);
                return {ok:true};
            }
        } catch(e){
            return {ok:false, msg:"`DError`: "+e.message};
        }
    }
    delete_at(context, path){
        path = path.replace(/\\/g, '\/');
        let parent = path.split('/');
        parent.pop();
        parent = parent.join('/');
        if(!this.hasPermission(context, path, 'write') && !this.hasPermission(context, parent, 'write')) return {ok:false, msg:"`DError`: Access Denied"};
        try {
            if(this.perm.get(path)) this.perm.delete(path);
            this.vol.unlinkSync(path);
            return {ok:true};
        } catch(e){
            return {ok:false, msg:"`DError`: "+e.message};
        }
    }
    read_file(context, path){
        path = path.replace(/\\/g, '\/');
        if(!this.hasPermission(context, path, 'read')) return {ok:false, msg:"`DError`: Access Denied"};
        try {
            if(this.vol.existsSync(path))
            {
                return {ok:true, data: this.vol.readFileSync(path, 'utf8')};
            }
            else return {ok:true, data: null};
        } catch(e){
            return {ok:false, msg:"`DError`: "+e.message};
        }
    }
    read_dir(context, path){
        path = path.replace(/\\/g, '\/');
        if(!this.hasPermission(context, path, 'read')) return {ok:false, msg:"`DError`: Access Denied"};
        try{
            return {ok:true, data: this.vol.readdirSync(path)};
        } catch(e){
            return {ok:false, msg:"`DError`: "+e.message};
        }
    }
    create_dir(context, path){
        path = path.replace(/\\/g, '\/');
        let parent = path.split('/');
        parent.pop();
        parent = parent.join('/');
        if(!this.hasPermission(context, parent, 'write')) return {ok:false, msg:"`DError`: Access Denied"};
        try {
            if(!fp.isAbsolute(path)) path = fp.normalize(context.PWD+'/'+path).replace(/\\/g, '\/');
            this.vol.mkdirpSync(path);
            if(!this.perm.get(path))
                this.perm.set(path, {owner:`${context.caller}:root`, write:[true, true, true], read:[true, true, true], exec:[false, false, false]});
            return {ok:true};
        } catch(e){
            return {ok:false, msg:"`DError`: "+e.message};
        }
    }
    install(path, packge){
        if(!fp.isAbsolute(path)) return {ok:false};
        
    }
    chmod(context, path, octal){
        // [1,1,1] -> ['exec','exec','exec']
        if(!path) return {ok:false, msg:"`DError`: Must provide a path"};
        if(!octal || isNaN(octal)) return {ok:false, msg:"`DError`: Must provide octal values"};
        var t = octal.split('');
        var valid = true;
        t.forEach(v=>{
            if(valid === false) return;
            if(Number(v) > 7 || Number(v) < 0)
            {
                valid = false;
            }
        });
        if(!valid) return {ok:false, msg:"`DError`: Octal values must be between 0 and 7"};
        path = path.replace(/\\/g,'\/');
        if(!fp.isAbsolute(path)) path = fp.normalize(context.PWD+'/'+path);
        if(this.vol.existsSync(path))
        {
            if(!this.hasPermission(context, path, 'write')) return {ok:false, msg:"`DError`: Access Denied"};
            try {
                const vals = [
                    '|exec;read;write',
                    'exec|read;write',
                    'write|read;exec',
                    'exec;write|read',
                    'read|exec;write',
                    'exec;read|write',
                    'read;write|exec',
                    'read;write;exec|'
                ]
                var perm = this.perm.get(path);
                if(!perm)
                {
                    this.perm.set(path, {owner:`${context.caller}:root`, write:[true, true, true], read:[true, true, true], exec:[false, false, false]});
                    perm = this.perm.get(path);
                }
                var ovals = octal.split('');
                ovals.forEach((val, i)=>{
                    val = vals[Number(val)];
                    val = val.split('|');
                    if(val[0])
                    {
                        val[0].split(';').forEach(el=>{
                            if(el != '')
                            {
                                perm[el][i] = true;
                            }
                        });
                    }
                    if(val[1])
                    {
                        val[1].split(';').forEach(el=>{
                            if(el != '')
                            {
                                perm[el][i] = false;
                            }
                        });
                    }
                });
                return {ok:true, msg:"Permissions set."};
            } catch(e){
                return {ok:false, msg:"`DError`: "+e.message};
            }
        }
        else return {ok:false, msg:"Path `N"+path+"` does not exist."};
    }
    run(context, path, paths){
        const self = this;
        try {
            if(this.vol.existsSync(path) && this.vol.statSync(path).isFile())
            {
                if(!this.hasPermission(context, path, 'exec') && context.caller != 'root') return {ok:false, msg:"`DError`: Access Denied"};
                return {ok:true, code: this.vol.readFileSync(path, 'utf8')};
            }
            else throw Error('');
        } catch(e){
            var res = null;
            paths.split(';').some(p=>{
                if(self.vol.existsSync(p+'/'+path) && self.vol.statSync(p+'/'+path).isFile())
                {
                    if(!self.hasPermission(context, p+'/'+path, 'exec')) return false;
                    res = self.vol.readFileSync(p+'/'+path, 'utf8');
                    return true;
                }
                return false;
            });
            if(res != null)
                return {ok:true, code: res};
            else
                return {ok:false, msg:"`DError`: Not found"};
        }
    }
}

module.exports.CPU = CPU;
module.exports.HDD = HDD;
module.exports.NetworkInterface = NetworkInterface;