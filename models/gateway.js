var inet = require('../modules/inet');
var software = require('./software');
class Gateway{
    constructor(save){
        const self = this;
        this.hwid = save.hwid;
        this.DHCP = new inet.DHCP(save.dhcp.block);
        this.vpm = new software.PackageManager(save.vpm);
        Object.keys(save.dhcp.mapped).forEach(key=>{
            var val = save.dhcp.mapped[key];
            this.DHCP.mapped.set(key, val);
        });
        Object.keys(save.dhcp.assigned).forEach(key=>{
            var val = save.dhcp.assigned[key];
            this.DHCP.assigned.set(key, val);
        });
        Object.keys(save.dhcp.expires).forEach(key=>{
            var val = save.dhcp.expires[key];
            this.DHCP.expires.set(key, val);
        });
    }
    beginPoll(){
        const self = this;
        setInterval(()=>{
            self.DHCP.mapped.forEach((hwid, address)=>{
                var machine = storage.machines.get(hwid);
                if(!machine)
                {
                    return;
                }
                var buffer = machine.nic.out.slice();
                machine.nic.out = [];
                buffer.forEach(packet=>{
                    self.route(packet);
                });
            });
            self.DHCP.expires.forEach((expiry, address)=>{
                if(Date.now() >= expiry)
                {
                    var machine = storage.machines.get(self.resolve(address));
                    machine.nic.assign('');
                    self.DHCP.release(address);
                    var addr = self.DHCP.assign(machine.hwid);
                    machine.nic.assign(addr);
                    machine.notify('Lease expired. Assigned IP: `N'+addr+'`');
                }
            });
        }, 1000);
    }
    toJObj(){
        return {
            hwid: this.hwid,
            dhcp: this.DHCP.toJObj(),
            vpm: this.vpm.toJObj()
        }
    }
    connect(username, hwid){
        let address = this.DHCP.assign(hwid);
        storage.dns.set(username+'.user', address)
        return address;
    }
    reconnect(hwid){
        return this.DHCP.retrieve(hwid);
    }
    resolve(host){
        let machine = storage.machines.get(this.DHCP.get(host));
        if(!machine)
        {
            host = storage.dns.resolve(host);
            if(host)
            {
                machine = storage.machines.get(this.DHCP.get(host));
                return machine;
            }
            else
            {
                return null;
            }
        }
        else return machine;
    }
    route(packet){
        try {
            console.log(this.hwid,'::ROUTING PACKET','=>',packet);
            if(packet.dest != 'broadcast')
            {
                var machine = this.resolve(packet.dest);
                if(!machine)
                {
                    return;
                };
                machine.nic.in.push(packet);
            }
            else
            {
                this.DHCP.assigned.forEach(address=>{
                    var machine = this.resolve(address);
                    if(!machine)
                    {
                        return;
                    };
                    var pack = Object.assign({}, packet);
                    pack.dest = address;
                    machine.nic.in.push(packet);
                });
            }
        }catch(e){
            console.error(e);
        }
    }
}

module.exports = Gateway;