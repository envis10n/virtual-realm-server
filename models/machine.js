const ivm = require('isolated-vm');
const fp = require('path');
const fs = require('fs-extra');
const commandLineArgs = require('command-line-args');
var inet = require('../modules/inet');
const bcrypt = require('bcryptjs');
const crypto = require('crypto');
var generateHWID = function(){
    return crypto.randomBytes(8).toString('hex')+'-'+crypto.randomBytes(16).toString('hex')+'-'+crypto.randomBytes(4).toString('hex');
}
const hardware = require('./hardware');
const Entities = require('html-entities').AllHtmlEntities;
const html = new Entities();

function copyObject(obj){
    return JSON.parse(JSON.stringify(obj));
}

function normalize(path, cwd){
    if(!fp.isAbsolute(path)) path = fp.normalize(cwd+'/'+path).replace(/\\/g,'\/');
    return path;
}

function parseString(ins){
    if(!(ins instanceof Array) && typeof ins == 'string') ins = ins.split(' ');
    else if(!(ins instanceof Array)) throw new Error('Input must be a string, or array of strings');
    var temp = [];
    var ti = -1;
    var ta = [];
    var sep = '';
    ins.forEach((val, i)=>{
        if(typeof val != 'string') val = val.toString();
        if(val[0] == '"' || val[0] == "'")
        {
            ti = i;
            ta.push(val.substring(1));
            sep = val[0];
        }
        else if(val[val.length-1] == sep && ti != -1)
        {
            ta.push(val.substring(0, val.length-1));
            ti = -1;
            sep = '';
            temp.push(ta.join(' '));
            ta = [];
        }
        else if(ti != -1)
        {
            ta.push(val);
        }
        else
        {
            temp.push(val);
        }
    });
    return temp;
}

class Shell {
    constructor(uuid, socket, machine){
        const self = this;
        this.uuid = uuid;
        this.username = '';
        this.PWD = '';
        this.PATH = '';
        this.HOME = '';
        this.groups = '';
        this.host = '';
        this.socket = socket;
        this.machine = machine;
        this.interpret = false;
        this.notifications = [];
        this.sudo_expire = 0;
    }
    command(data, script = false){
        const self = this;
        var sudo = false;
        if(data.split(' ')[0] == 'sudo')
        {
            if(self.username == 'root')
            {
                return '`DError`: Root user cannot use sudo';
            }
            if(self.sudo_expire <= Date.now())
            {
                return self.sudo(dobj);
            }
            else
            {
                sudo = true;
                self.sudo_expire = Date.now()+(1000*60*15);
                data = data.split(' ').slice(1).join(' ');
            }
        }
        var dcmd = data.split(' ')[0].toLowerCase();
        switch(dcmd){
            case 'rm':
                data = data.split(' ').slice(1);
                data = parseString(data);
                var args = commandLineArgs([
                    {name:"recursive", alias:"R", type: Boolean, defaultValue: false},
                    {name:"path", type: String, defaultOption: true}
                ], {argv: data});
                args.path = normalize(args.path, self.PWD);
                return self.machine.hdd.remove_data(args.path, args.recursive);
            break;
            case 'cd':
                if(script) return;
                data = data.split(' ').slice(1);
                data = parseString(data);
                var args = commandLineArgs([
                    {name:"path", type: String, defaultOption: true}
                ], {argv: data});
                if(!args.path) return;
                else
                {
                    args.path = args.path.replace('~', self.getContext(sudo).HOME);
                    var npath = normalize(args.path, self.PWD);
                    if(self.machine.hdd.vol.existsSync(npath))
                    {
                        self.PWD = npath;
                        self.sendJSON({op:"PROMPT", prompt: self.getPrompt(), input:{password: false}});
                        return;
                    }
                    else return `\`DError\`: Path ${npath} does not exist`;
                }
            break;
            case 'vpm':
                if(script) return;
                var sub = data.split(' ')[0];
                data = data.split(' ').slice(1).join(' ');
                switch(sub){
                    case 'install':

                    break;
                    case 'update':

                    break;
                    case 'remove':

                    break;
                    case 'publish':

                    break;
                    case 'unpublish':

                    break;
                    default:

                    break;
                }
            break;
            case 'visudo':
                if(script) return;
                if(!self.username == 'root')
                {
                    return 'You must run this command as root, not via sudo.';
                }
                data = data.split(' ').slice(1);
                data = parseString(data);
                var args = commandLineArgs([
                    {name:"username", type: String, defaultOption: true},
                    {name:"sudo", alias:"s", type: Boolean, defaultValue: true}
                ], {argv: data});
                args.sudo = Boolean(args.sudo);
                self.machine.hdd.set_sudo(args.username, args.sudo);
                return 'User permissions updated.';
            break;
            case 'adduser':
                if(script) return;
                if(!sudo && self.username != 'root')
                {
                    return 'You must run this command as root';
                }
                data = data.split(' ').slice(1);
                data = parseString(data);
                var args = commandLineArgs([
                    {name:"username", type: String, defaultOption: true}
                ], {argv: data});
                if(!args.username)
                {
                    return '`DError`: You must provide a username';
                }
                if(self.machine.hdd.user_exists(args.username).ok === true)
                {
                    self.sendJSON({op:"PROMPT", prompt: self.getPrompt(),})
                    return '`DError`: User already exists';
                }
                function newuserPass(){
                    var npassid1 = self.socket.newPrompt('npass1', function(password1){
                        var npassid2 = self.socket.newPrompt('npass2', function(password2){
                            self.socket.sendJSON({op:"PROMPT", prompt:"", input:{password: false}});
                            if(password1 != password2)
                            {
                                self.socket.sendResult('Passwords did not match');
                                newuserPass();
                            }
                            else
                            {
                                self.machine.hdd.add_user(args.username, password2);
                                self.socket.sendResult(`New user \`N${args.username}\` created`);
                                self.sendJSON({op:"PROMPT", prompt: self.getPrompt(), input:{password: false}});
                            }
                        });
                        self.sendJSON({op:"PROMPT", prompt: "Re-enter password:", input:{password: true, id: npassid2}});
                        self.socket.sendResult();
                    });
                    self.sendJSON({op:"PROMPT", prompt: "Enter password:", input:{password: true, id: npassid1}});
                    self.socket.sendResult();
                }
                newuserPass();
            break;
            case 'ls':
                var context = self.getContext(sudo);
                var path = data.split(' ')[1] ? data.split(' ')[1] : context.PWD;
                if(!fp.isAbsolute(path)) path = context.PWD+'/'+path;
                path = fp.normalize(path);
                if(!self.machine.hdd.vol.existsSync(path))
                {
                    return '`DError`: Path does not exist.';
                }
                var fstat = self.machine.hdd.vol.statSync(path);
                if(fstat.isFile())
                {
                    var d = self.machine.hdd.perm.get(path);
                    if(!d)
                    {
                        return '';
                    }
                    else
                    {
                        return JSON.stringify(d);
                    }
                }
                else
                {
                    var files = self.machine.hdd.vol.readdirSync(path);
                    if(files instanceof Array)
                    {
                        return {msg: files.map(file=>{
                            var stat = self.machine.hdd.vol.statSync(path+'/'+file);
                            var color = stat.isDirectory() ? 'P' : '1';
                            return `\`${color}${file}\``;
                        }).join('  '), files: files};
                    }
                    else
                    {
                        return {msg: '', files: []};
                    }
                }
            break;
            case 'chmod':
                var context = self.getContext(sudo);
                var octal = data.split(' ')[1];
                var path = data.split(' ')[2];
                return self.machine.hdd.chmod(context, path, octal);
            break;
            case 'ifconfig':
                var subcom = data.split(' ')[1];
                switch(subcom){
                    case 'lease':
                        var expires = new Date(storage.gateways.get(self.machine.nic.gateway).DHCP.expires.get(self.machine.nic.address));
                        return `Address lease expires on ${expires.toLocaleDateString()} at ${expires.toLocaleTimeString()}`;
                    break;
                    case undefined:
                        var addr = self.machine.nic.address;
                        var gateway = self.machine.nic.gateway;
                        return `Address [${addr}] : Gateway [${gateway}]`;
                    break;
                }
            break;
            case 'passwd':
                if(script) return;
                if(!sudo && !self.username == 'root')
                {
                    return 'You must run this command as root';
                }
                data = data.split(' ').slice(1);
                data = parseString(data);
                var args = commandLineArgs([
                    {name:"username", type: String, defaultOption: true},
                    {name:"expire", alias:"e", type: Boolean, defaultValue:false}
                ], {argv: data, partial: true});
                if(args.expire === true)
                {
                    if(self.machine.hdd.user_exists(args.username))
                    {
                        self.machine.hdd.expire_password(args.username);
                        return 'User password expiration updated.';
                    }
                    else
                    {
                        return '`DError`: User does not exist.';
                    }
                }
                else
                {
                    function newuserPass(){
                        var npassid1 = self.socket.newPrompt('npass1', function(password1){
                            var npassid2 = self.socket.newPrompt('npass2', function(password2){
                                self.socket.sendJSON({op:"PROMPT", prompt:"", input:{password: false}});
                                if(password1 != password2)
                                {
                                    self.socket.sendResult('Passwords did not match');
                                    newuserPass();
                                }
                                else
                                {
                                    self.machine.hdd.set_password(args.username, password2);
                                    self.socket.sendResult(`Password for \`N${args.username}\` updated.`);
                                    self.sendJSON({op:"PROMPT", prompt: self.getPrompt(), input:{password: false}});
                                }
                            });
                            self.sendJSON({op:"PROMPT", prompt: "Re-enter new user password:", input:{password: true, id: npassid2}});
                            self.socket.sendResult();
                        });
                        self.sendJSON({op:"PROMPT", prompt: "Enter new user password:", input:{password: true, id: npassid1}});
                        self.socket.sendResult();
                    }
                    newuserPass();
                }
            break;
            case 'mkdir':
                data = data.split(' ').slice(1);
                data = parseString(data);
                var args = commandLineArgs([
                    {name:"path", type: String, defaultOption: true}
                ], {argv: data});
                if(!args.path) return;
                else return self.machine.hdd.create_dir(self.getContext(sudo), args.path);
            break;
            default:
                var ret = self.run(data, sudo);
                return ret;
            break;
        }
    }
    sudo(command){
        const self = this;
        var cs = self.machine.hdd.can_sudo(self.username);
        if(cs.ok === false)
        {
            self.socket.sendResult('`DError`: Access Denied');
        }
        else
        {
            var passid = this.socket.newPrompt('sudopass', function(password){
                self.socket.sendJSON({op:"PROMPT", prompt:"", input:{password: false}});
                var ret = self.machine.hdd.check_pass(self.username, password);
                if(ret.ok === true)
                {
                    self.sudo_expire = Date.now()+(1000*60*15);
                    self.socket.sendJSON({op:"PROMPT", prompt: self.getPrompt(), input:{password: false}});
                    self.socket.onData(self.socket, JSON.stringify(command));
                }
                else
                {
                    self.socket.sendResult('`DError`: Access denied');
                    self.socket.sendJSON({op:"PROMPT", prompt: self.getPrompt(), input:{password: false}});
                }
            });
            self.socket.sendResult();
            self.socket.sendJSON({op:"PROMPT", prompt: "[sudo] enter password:", input:{password: true, id: passid}});
        }
    }
    login(ip = 'localhost'){
        const self = this;
        var usernameid = this.socket.newPrompt('username', function(username){
            var passwordid = self.socket.newPrompt('password', function(password){
                self.socket.sendJSON({op:"PROMPT", prompt:"", input:{password: false}});
                var res = self.machine.hdd.auth_user(username, password);
                console.log(res);
                if(res.ok === false && res.reset === true)
                {
                    self.HOME = res.home;
                    self.PWD = res.home;
                    self.PATH = '/bin;';
                    self.groups = res.groups;
                    self.username = username;
                    self.host = ip;
                    var info = `Last login: ${new Date(res.last_login).toUTCString()} from ${res.last_ip}`;
                    self.socket.sendResult('`2Access Granted`\n'+info+'\n\nPassword expired. Please `Vreset` password.');
                    self.machine.hdd.login_user(self.username, ip);
                    self.reset_password();
                }
                else if(res.ok === false)
                {
                    self.socket.sendResult('`DAccess Denied`');
                    self.login(ip);
                }
                else if(res.ok === true)
                {
                    self.HOME = res.home;
                    self.PWD = res.home;
                    self.PATH = '/bin;';
                    self.groups = res.groups;
                    self.username = username;
                    self.host = ip;
                    var info = `Last login: ${new Date(res.last_login).toUTCString()} from ${res.last_ip}`;
                    self.socket.sendResult('`2Access Granted`\n'+info);
                    self.machine.hdd.login_user(self.username, ip);
                    self.socket.sendJSON({op:"PROMPT", prompt: self.getPrompt(), input:{password: false}});
                }
            });
            self.socket.sendJSON({op:"PROMPT", prompt: `Enter password for \`N${username}\`@\`V${ip}\`:`, input:{password: true, id: passwordid}});
        });
        this.socket.sendJSON({op:"PROMPT", prompt: 'login as:', input:{password: false, id: usernameid}});
    }
    reset_password(){
        const self = this;
        var usernameid = this.socket.newPrompt('password1', function(password1){
            var passwordid = self.socket.newPrompt('password2', function(password2){
                self.socket.sendJSON({op:"PROMPT", prompt:"", input:{password: false}});
                if(password1 != password2)
                {
                    self.socket.sendResult('`DError`: Passwords did not match');
                    self.reset_password();
                }
                else
                {
                    var res = self.machine.hdd.set_password(self.username, password2);
                    if(res.ok === false)
                    {
                        self.socket.sendResult('`DError`: An error occurred');
                        self.reset_password();
                    }
                    else if(res.ok === true)
                    {
                        self.socket.sendResult('`2Password Updated`');
                        self.socket.sendJSON({op:"PROMPT", prompt: self.getPrompt(), input:{password: false}});
                    }
                }
            });
            self.socket.sendJSON({op:"PROMPT", prompt: "Re-enter password:", input:{password: true, id: passwordid}});
        });
        this.socket.sendJSON({op:"PROMPT", prompt: 'Enter password:', input:{password: true, id: usernameid}});
    }
    getPrompt(){
        return `\`N${this.username}\`@\`V${this.host}\`:\`C${(this.PWD.replace(this.HOME, '~'))}\`$`;
    }
    tick(){
        const self = this;
        if(self.notifications.length > 0)
        {
            let buffer = self.notifications.slice(0);
            self.notifications = [];
            buffer.forEach((payload)=>{
                self.sendJSON(payload);
            });
        }
    }
    disconnect(){
        this.machine.shells.delete(this.uuid);
    }
    getContext(sudo = false){
        return {
            PWD: this.PWD,
            HOME: this.HOME,
            PATH: this.PATH,
            caller: (sudo ? 'root' : this.username),
            groups: (sudo ? 'root' : this.groups),
            display: this.display,
            uuid: this.uuid
        }
    }
    run(command, sudo = false){
        const self = this;
        if(command.toLowerCase() == 'js')
        {
            this.interpret = true;
            return {ok:true, msg:"`VJS Interpreter Active`"};
        }
        else if(this.interpret)
        {
            if(command.toLowerCase() == 'exit')
            {
                this.interpret = false;
                return {ok:true, msg:"`VShell Active`"};
            }
            else
            {
                var ret = this.machine.eval(this.getContext(sudo), command);
                return ret;
            }
        }
        else
        {
            return this.machine.run(this, command, sudo);
        }
    }
    sendJSON(obj){
        this.socket.sendJSON(obj);
    }
    sendRetval(data, success = true){
        let obj = {
            success: success,
            data:{
                retval: data
            }
        }
        this.socket.sendJSON(obj);
    }
}

class Machine {
    constructor(save){
        this.hwid = save.hwid;
        this.cpu = new hardware.CPU(save.cpu.speed, save.cpu.cores);
        this.hdd = new hardware.HDD(save.drive);
        this.nic = new hardware.NetworkInterface(save.network);
        this.isolate = new ivm.Isolate({memoryLimit: 15});
        this.shells = new Map();
        this.notifications = save.notifications ? save.notifications : [];
        this.lt = 0;
        this.tInt = null;
        this.realtimes = new Map();
        this.software = new Map();
        var soft = save.software || {};
        Object.keys(soft).forEach(key=>{
            var val = soft[key];
            this.software.set(key, val);
        });
    }
    install(soft){

    }
    notify(message){
        if(this.shells.size > 0)
        {
            this.shells.forEach(shell=>{
                shell.notifications.push(message);
            });
        }
        else
        {
            this.notifications.push(message);
        }
    }
    toJObj(){
        var software = Array.from(this.software);
        var sobj = {};
        software.forEach(el=>{
            sobj[el[0]] = el[1];
        });
        return {
            hwid: this.hwid,
            cpu: this.cpu.toJSON(),
            drive: this.hdd.toJSON(),
            network: this.nic.toJSON(),
            notifications: this.notifications,
            software: sobj
        }
    }
    logConnection(address){
        this.hdd.vol.appendFileSync('/sys/access.log', JSON.stringify({ts: Date.now(), address: address})+'\n');
    }
    realtimeCreate(shell){
        var id = crypto.randomBytes(16).toString('hex');
        while(this.realtimes.get(id) != undefined)
        {
            id = crypto.randomBytes(16).toString('hex');
        }
        var sh = this.shells.get(shell);
        if(!sh) return false;
        this.realtimes.set(id, {shell: sh, enabled: false});
        return id;
    }
    realtimeInit(id, cb){
        var rt = this.realtimes.get(id);
        if(!rt || rt.enabled === true) return false;
        var sh = rt.shell;
        rt.enabled = true;
        sh.sendJSON({success:true, op:"REALTIME", create:true, run_id: id});
        return true;
    }
    boot(code, context){
        const self = this;
        if(context.uuid != undefined)
            var run_id = self.realtimeCreate(context.uuid);
        else
            var run_id = crypto.randomBytes(16).toString('hex');
        context.run_id = run_id;
        // BOOT
        let vmcontext = self.isolate.createContextSync();
        let glob = vmcontext.global;
        glob.setSync('global', glob.derefInto());
        glob.setSync('_ivm', ivm);
        glob.setSync('_shell', new ivm.Reference(function(command){
            if(command.split(' ')[0] == 'sudo')
            {
                return {ok:false, msg:"`DError`: Shell commands called from a script cannot use sudo."};
            }
            else
            {
                return JSON.stringify(self.shells.get(context.uuid).command(command, true));
            }
        }));
        glob.setSync('_parse', new ivm.Reference(function(options, args, partial = false){
            return JSON.stringify(commandLineArgs(JSON.parse(options), {argv: JSON.parse(args), partial: partial}));
        }));
        glob.setSync('_netGate', new ivm.Reference(function(){
            return self.nic.gateway;
        }));
        glob.setSync('_rtCreate', new ivm.Reference(function(id, draw){
            return self.realtimeInit(id, draw);
        }));
        glob.setSync('_rtUpdate', new ivm.Reference(function(id, val){
            self.realtimeUpdate(id, val);
        }));
        glob.setSync('_rFS', new ivm.Reference(function(context, path){
            return JSON.stringify(self.hdd.read_file(context, path));
        }));
        glob.setSync('_wFS', new ivm.Reference(function(context, path, data){
            return JSON.stringify(self.hdd.write_file(context, path, data));
        }));
        glob.setSync('_aFS', new ivm.Reference(function(context, path, data){
            return JSON.stringify(self.hdd.append_file(context, path, data));
        }));
        glob.setSync('_rdS', new ivm.Reference(function(context, path){
            return JSON.stringify(self.hdd.read_dir(context, path));
        }));
        glob.setSync('_rS', new ivm.Reference(function(context, path){
            return JSON.stringify(self.hdd.delete_at(context, path));
        }));
        glob.setSync('_usage', new ivm.Reference(function(){
            return self.hdd.usage();
        }));
        glob.setSync('_normalizePath', new ivm.Reference(function(path){
            return fp.normalize(path);
        }));
        glob.setSync('_isAbsolute', new ivm.Reference(function(path){
            return fp.isAbsolute(path);
        }));
        glob.setSync('_mDS', new ivm.Reference(function(context, path){
            return JSON.stringify(self.hdd.create_dir(context, path));
        }));
        glob.setSync('_encode', new ivm.Reference(function(...args){
            return html.encode(args.join(' '));
        }));
        glob.setSync('_rtUpdate', new ivm.Reference(function(id, val){
            var rt = self.realtimes.get(id);
            if(!rt) return;
            var sh = rt.shell;
            if(!sh) return;
            if(!rt.enabled) return;
            sh.sendJSON({success:true, op:"REALTIME", run_id: id, val: JSON.stringify(val)});
        }));
        glob.setSync('_notify', new ivm.Reference(function(payload, shell){
            if(payload == undefined) throw new Error("Payload must be set.");
            let obj = {t: Date.now(), success: true, data:{ retval: payload }};
            if(!shell)
            {
                if(self.shells.size > 0)
                {
                    self.shells.forEach(shell=>{
                        shell.notifications.push(copyObject(obj));
                    });
                }
                else
                {
                    self.notifications.push(copyObject(obj));
                }
            }
            else
            {
                if(self.shells.size > 0)
                {
                    self.shells.forEach(sh=>{
                        if(sh.uuid == shell || sh.username == shell)
                        {
                            sh.notifications.push(copyObject(obj));
                        }
                    });
                }
                else
                {
                    self.notifications.push(copyObject(obj));
                }
            }
            return payload;
        }));
        // bootstrap
        let bootstrap = self.isolate.compileScriptSync(`((context)=>{
            let script_end = Date.now()+14900;
            context = JSON.parse(context);
            let HOME = context.HOME;
            let PWD = context.PWD;
            let PATH = context.PATH;
            var isRealtime = false;
            let run_id = context.run_id;
            Object.freeze(context);
            (()=>{
                let ivm = _ivm;
                delete _ivm;
                let netGate = _netGate;
                let rFS = _rFS;
                let wFS = _wFS;
                let aFS = _aFS;
                let rS = _rS;
                let rdS = _rdS;
                let usage = _usage;
                let mDS = _mDS;
                let normalizePath = _normalizePath;
                let isAbsolute = _isAbsolute;
                let encode = _encode;
                let notify = _notify;
                let rtCreate = _rtCreate;
                let rtUpdate = _rtUpdate;
                let parse = _parse;
                let shell = _shell;
                delete _shell;
                delete _parse;
                delete _rtUpdate;
                delete _rtCreate;
                delete _encode;
                delete _netGate;
                delete _netBroadcast;
                delete _notify;
                delete _mDS;
                delete _isAbsolute;
                delete _normalizePath;
                delete _usage;
                delete _rFS;
                delete _wFS;
                delete _aFS;
                delete _rS;
                delete _rdS;
                Function.prototype.toString = function(){ return '[function Function]' };
                Object.defineProperty(global, 'can_continue_execution', {
                    get: function(){
                        return (Date.now() < script_end);
                    },
                    set: function(){}
                });
                global.parseArgs = function(options, argv, partial = false){
                    var args = [];
                    args.push(JSON.stringify(options));
                    args.push(JSON.stringify(argv));
                    args.push(partial);
                    return JSON.parse(parse.applySync(undefined, args.map(arg=>{ return new ivm.ExternalCopy(arg).copyInto()})));
                }
                global.sh = function(command){
                    var args = [];
                    args.push(new ivm.ExternalCopy(command).copyInto());
                    return JSON.parse(shell.applySync(undefined, args));
                }
                global.html = {};
                global.html.encode = function(...args){
                    return encode.applySync(undefined, args.map(arg=>{ return new ivm.ExternalCopy(arg).copyInto()}));
                }
                global.realtime = {};
                global.require = function(path){
                    var file = fs.readFileSync(path);
                    if(typeof file == 'string')
                    {
                        return new Function(file)();
                    }
                    else return null;
                }
                global.realtime.init = function(cb){
                    if(isRealtime) return;
                    let args = [];
                    args.push(new ivm.ExternalCopy(run_id).copyInto());
                    rtCreate.applySync(undefined, args);
                    isRealtime = true;
                    var rt = 0;
                    var frame = 16;
                    while(global.can_continue_execution){
                        if(Date.now() >= rt)
                        {
                            var ret = cb();
                            if(typeof ret == 'object') ret = JSON.stringify(ret);
                            if(typeof ret == 'function') ret = '[function Function]';
                            var uargs = [];
                            uargs.push(new ivm.ExternalCopy(run_id).copyInto());
                            uargs.push(new ivm.ExternalCopy(ret).copyInto());
                            rtUpdate.applySync(undefined, uargs);
                            rt = Date.now()+frame;
                        }
                    }
                    return;
                }
                global.fs = {};
                global.fs.normalize = function(...args){
                    return normalizePath.applySync(undefined, args.map(arg=>{ return new ivm.ExternalCopy(arg).copyInto()}));
                }
                global.fs.isAbsolute = function(...args){
                    return isAbsolute.applySync(undefined, args.map(arg=>{ return new ivm.ExternalCopy(arg).copyInto()}));
                }
                global.fs.readFileSync = function(...args){
                    let temp = args.slice(0);
                    args = [];
                    args.push(context);
                    temp.forEach(el=>{
                        args.push(el);
                    });
                    delete temp;
                    var ret = rFS.applySync(undefined, args.map(arg=>{ return new ivm.ExternalCopy(arg).copyInto()}));
                    ret = JSON.parse(ret);
                    if(ret.ok) return ret.data;
                    else return ret;
                }
                global.fs.writeFileSync = function(...args){
                    let temp = args.slice(0);
                    args = [];
                    args.push(context);
                    temp.forEach(el=>{
                        args.push(el);
                    });
                    delete temp;
                    var ret = wFS.applySync(undefined, args.map(arg=>{ return new ivm.ExternalCopy(arg).copyInto()}));
                    ret = JSON.parse(ret);
                    if(ret.ok) return ret.data;
                    else return ret;
                }
                global.fs.appendFileSync = function(...args){
                    let temp = args.slice(0);
                    args = [];
                    args.push(context);
                    temp.forEach(el=>{
                        args.push(el);
                    });
                    delete temp;
                    var ret = aFS.applySync(undefined, args.map(arg=>{ return new ivm.ExternalCopy(arg).copyInto()}));
                    ret = JSON.parse(ret);
                    if(ret.ok) return ret.data;
                    else return ret;
                }
                global.fs.removeSync = function(...args){
                    let temp = args.slice(0);
                    args = [];
                    args.push(context);
                    temp.forEach(el=>{
                        args.push(el);
                    });
                    delete temp;
                    var ret = rS.applySync(undefined, args.map(arg=>{ return new ivm.ExternalCopy(arg).copyInto()}));
                    ret = JSON.parse(ret);
                    if(ret.ok) return ret.data;
                    else return ret;
                }
                global.fs.readdirSync = function(...args){
                    let temp = args.slice(0);
                    args = [];
                    args.push(context);
                    temp.forEach(el=>{
                        args.push(el);
                    });
                    delete temp;
                    var ret = rdS.applySync(undefined, args.map(arg=>{ return new ivm.ExternalCopy(arg).copyInto()}));
                    ret = JSON.parse(ret);
                    if(ret.ok) return ret.data;
                    else return ret;
                }
                global.fs.usage = function(){
                    var ret = usage.applySync();
                    return JSON.parse(ret);
                }
                global.fs.mkdirpSync = function(...args){
                    let temp = args.slice(0);
                    args = [];
                    args.push(context);
                    temp.forEach(el=>{
                        args.push(el);
                    });
                    delete temp;
                    var ret = mDS.applySync(undefined, args.map(arg=>{ return new ivm.ExternalCopy(arg).copyInto()}));
                    ret = JSON.parse(ret);
                    if(ret.ok)
                    {
                        if(ret.data != undefined)
                            return ret.data;
                        else return ret;
                    }
                    else return ret;
                }
                global.net = {};
                global.notify = function(...args){
                    return notify.applySync(undefined, args.map(arg=>{ return new ivm.ExternalCopy(arg).copyInto()}));
                }    
                Object.defineProperty(global.net, 'gateway', {
                    get: function(){
                        return netGate.applySync();
                    },
                    set: function(){
                        return undefined;
                    }
                });
                Object.freeze(global.realtime);
                Object.freeze(global.html);
                Object.freeze(global.net);
                Object.freeze(global.fs);
                Object.freeze(global);
            })();
            return (()=>{
                ${code};
            })();
        })('${JSON.stringify(context)}')`);
        let ret = bootstrap.runSync(vmcontext, {timeout: 15000});
        vmcontext.release();
        this.realtimes.delete(context.run_id);
        return ret;
    }
    handlePacket(packet){
        var service = '/services/network/'+packet.port+'.service';
        if(this.hdd.vol.existsSync(service))
        {
            var scode = this.hdd.vol.readFileSync(service, 'utf8');
            var args = {
                payload: packet.payload
            };
            var code = `var run = ${scode};
            var args = '${JSON.stringify(args)}';
            args = JSON.parse(args);
            var ret = run(context, args);
            if(ret == undefined) ret = null;
            ret = JSON.stringify(ret);
            return ret;`
            var context = {
                caller: 'system',
                address: packet.source
            };
            try {
                var ret = JSON.parse(this.boot(code, context));
                if(ret == undefined) ret = 'null';
                return ret;
            } catch(e){
                this.notify(`\`DError\`: Service for port ${packet.port} returned an error: ${e.message}`);
                return 'null';
            }
        }
        else
        {
            return;
        }
    }
    beginTick(){
        const self = this;
        this.tInt = setInterval(()=>{
            self.tick();
        }, this.cpu.speed);
    }
    tick(){
        const self = this;
        // System tick
        this.lt = Date.now();
        //Software loop
        if(Array.from(this.software).length > 0)
        {
            this.software.forEach(soft=>{
                this.boot(soft.code, soft.context);
            });
        }
        let net_in = self.nic.receive();
        if(net_in != null && net_in.length > 0)
        {
            net_in.forEach(packet=>{
                let service = self.nic.services.get(packet.port);
                if(!service) return;
                self.run_script(service.code, service.context, [packet.payload]);
            });
        }
        if(this.notifications.length > 0)
        {
            if(self.shells.size > 0)
            {
                var t = self.notifications.slice(0);
                self.notifications = [];
                t.forEach(message=>{
                    self.shells.forEach(shell=>{
                        shell.notifications.push(message);
                    });
                });
            }
        }
        self.shells.forEach(shell=>{
            shell.tick();
        });
    }
    toJSON(){
        return {
            cpu: this.cpu.toJSON(),
            drive: this.hdd.toJSON()
        }
    }
    eval(context, code){
        try {
            let ucode = `function run(){
                return ${code}
            }
            var ret = run();
            if(ret == undefined) ret = null;
            ret = JSON.stringify(ret);
            return ret;`
            let ret = this.boot(ucode, context);
            ret = JSON.parse(ret);
            return ret;
        } catch(e){
            console.error(e);
            return {ok:false, msg: e.message};
        }
    }
    run_script(code, context, args){
        console.log(code);
        code = `var run = ${code};
        var args = '${JSON.stringify(args)}';
        args = JSON.parse(args);
        var ret = run(context, args);
        if(ret == undefined) ret = null;
        ret = JSON.stringify(ret);
        return ret;`
        try {
            let ret = this.boot(code, context);
            ret = JSON.parse(ret);
            return ret;
        } catch(e){
            console.log(e);
            console.log("ERROR ON CODE SCRIPT: ", code);
            return "Error: "+e.message;
        }
    }
    run(shell, command, sudo = false){
        function parseString(ins){
            if(!(ins instanceof Array) && typeof ins == 'string') ins = ins.split(' ');
            else if(!(ins instanceof Array)) throw new Error('Input must be a string, or array of strings');
            var temp = [];
            var ti = -1;
            var ta = [];
            var sep = '';
            ins.forEach((val, i)=>{
                if(typeof val != 'string') val = val.toString();
                if(val[0] == '"' || val[0] == "'")
                {
                    ti = i;
                    ta.push(val.substring(1));
                    sep = val[0];
                }
                else if(val[val.length-1] == sep && ti != -1)
                {
                    ta.push(val.substring(0, val.length-1));
                    ti = -1;
                    sep = '';
                    temp.push(ta.join(' '));
                    ta = [];
                }
                else if(ti != -1)
                {
                    ta.push(val);
                }
                else
                {
                    temp.push(val);
                }
            });
            return temp;
        }
        const self = this;
        try {
            var segs = command.split(' ');
            var args = segs;
            args = parseString(args);
            args = args.map(arg=>{
                return arg.replace('%PWD%', shell.PWD).replace('%PATH%', shell.PATH).replace('%HOME%', shell.HOME);
            });
            var cmd = args[0];
            args = args.slice(1);
            var path = cmd;
            var code = null;
            if(!(new RegExp(/[.\/\\]/).test(path)))
            {
                var d = this.hdd.run(shell.getContext(sudo), cmd, shell.PATH);
                if(d.ok) code = d.code;
                else code = null;
            }
            else
            {
                if(!fp.isAbsolute(path)) path = normalize(path, shell.PWD);
                if(self.hdd.vol.existsSync(path) && self.hdd.vol.statSync(path).isFile())
                {
                    var d = this.hdd.run(shell.getContext(sudo), path, shell.PATH);
                    if(d.ok) code = d.code;
                    else code = null;
                }
                else code = null;
            }
            if(code == null)
            {
                return `\`DError\`: ${cmd} unknown command or operation.`;
            }
            else if(code != '')
            {
                var ret = this.run_script(code, shell.getContext(sudo), args);
                return ret;
            }
            else
            {
                return `\`DError\`: ${cmd} unknown command or operation.`;
            }
        } catch(e){
            return `\`DError\`: ${cmd} unknown command or operation.`;
        }
    }
    shell(uuid, socket, address = 'localhost'){
        var hook = this.run_hook('connection', {address: address, port: 22});
        if(!hook)
        {
            return null;
        }
        this.logConnection(address);
        var shook = this.run_hook('ssh', {address: address});
        if(!shook)
        {
            return null;
        }
        const self = this;
        var shell = new Shell(uuid, socket, self);
        this.shells.set(uuid, shell);
        return shell;
    }
    run_hook(name, event){
        const self = this;
        try {
            var root = `/sys/hook/${name}`;
            if(this.hdd.vol.existsSync(root) && this.hdd.vol.statSync(root).isDirectory())
            {
                var files = this.hdd.vol.readdirSync(root);
                files.sort((a, b)=>{
                    var n1 = Number(a.split('_')[0]);
                    var n2 = Number(b.split('_')[0]);
                    return (n1 > n2 ? 1 : (n1 < n2 ? -1 : 0));
                });
                console.log(files);
                var max = files.length;
                var count = 0;
                console.log(`[HOOK::${name}] Found ${files.length} hook definition(s)`);
                files.some(fname=>{
                    if(this.hdd.vol.statSync(root+'/'+fname).isFile())
                    {
                        var code = this.hdd.vol.readFileSync(root+'/'+fname, 'utf8');
                        code = `var run = ${code}
                        var e = '${JSON.stringify(event)}';
                        var ret = JSON.stringify(run(JSON.parse(e)));
                        return ret;`;
                        var ret = JSON.parse(this.boot(code, {caller: 'root', groups: 'root', PWD: '/', HOME: '/', PATH: '/bin;'}));
                        console.log(`[HOOK::${name}]::[${fname}] => ${ret}`);
                        if(ret === false)
                        {
                            console.log(`[HOOK::${name}]::[${fname}] => Hook failed. Breaking...`);
                            return true;
                        }
                        else
                        {
                            console.log(`[HOOK::${name}]::[${fname}] => Hook successful. Continuing...`);
                            count += 1;
                            return false;
                        }
                    }
                    else
                    {
                        console.log(`[HOOK::${name}]::[${fname}] => Hook failed. Path is not a file.`);
                        return true;
                    }
                });
                return count == max;
            }
            else return false;
        } catch(e){
            console.error(e);
            return false;
        }
    }
    killShell(uuid){
        this.shells.delete(uuid);
    }
    poll(){
        return this.nic.read();
    }
    static generate(username){
        let passwd = `{
            "root":{
                "password": "${bcrypt.hashSync('root', bcrypt.genSaltSync(13))}",
                "groups":"root",
                "sudo": true,
                "home":"/",
                "expiry": 0,
                "last_login": 0,
                "last_ip":"",
                "can_ssh": true,
                "logged_in": false
            }
        }`;
        let machine = new Machine({
            hwid: generateHWID().toString(),
            cpu:{
                speed: 2500,
                cores: 2
            },
            drive:{
                size: 10 * 1024 * 1024,
                perm:{
                    '/etc/passwd': {owner:'root:root', write:[true, true, false], read:[true, true, false], exec:[false, false, false]},
                    '/bin': {owner:"root:root", write:[true, true, false], read:[true, true, true], exec:[false, false, false]}
                },
                hdd:{
                    '/etc/passwd': passwd,
                    '/sys/access.log':'',
                    '/bin': null,
                    '/sys/hook': null
                }
            },
            network:{
                buffer: 20,
                gateway: 'vrealm-gateway-0000',
                proxy: null,
                address: '',
                in: [],
                out: []
            },
            notifications: []
        });
        // Generate initial hooks
        function getHooks(dir){
            try {
                dir = process.cwd()+'/system_hooks/'+dir;
                let files = fs.readdirSync(dir);
                let temp = [];
                files.forEach(file=>{
                    let path = dir+'/'+file;
                    if(fs.statSync(path).isFile())
                    {
                        temp.push({name: file, code: fs.readFileSync(path).toString()});
                    }
                });
                return temp;
            } catch(e){
                return [];
            }
        }
        const hooks = {
            'connection': getHooks('connection'),
            'ssh': getHooks('ssh')
        }
        Object.keys(hooks).forEach(key=>{
            var context = {caller: 'root', groups: 'root', PWD: '/', HOME: '/'};
            var val = hooks[key];
            machine.hdd.create_dir(context, `/sys/hook/${key}`);
            val.forEach(file=>{
                machine.hdd.write_file(context, `/sys/hook/${key}/${file.name}`, file.code);
            });
        });
        machine.beginTick();
        return machine;
    }
}

module.exports = Machine;