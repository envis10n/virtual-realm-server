var items = storage.items;
class Item {
    constructor(template_id){
        var item = items.get(template_id);
        if(!item) throw new Error('Item ID invalid');
        this.id = item.id;
        this.display = item.display;
    }
}