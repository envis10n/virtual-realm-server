class PackageManager extends Map{
    constructor(save){
        super();
        if(!save) save = [];
        save.forEach(packaged=>{
            this.set(packaged.name, packaged);
        });
    }
    toJObj(){
        var temp = [];
        Array.from(this).forEach(el=>{
            temp.push(el[1]);
        });
        return temp;
    }
    check_update(name, version){
        var packge = this.get(name);
        if(!packge) return {ok:false, error: true, msg:`Package \`V${name}\` not found`};
        if(packge.latest_version == version) return {ok:false, msg:`Package up-to-date`};
        else
        {
            packge = packge.versions[version];
            return {ok:true, version: packge.version, packge: packge};
        }
    }
    publish(user, name, pack){
        var packge = this.get(name);
        if(packge != undefined && packge.owner != user) return {ok: false, error: true, msg:`Package \`V${name}\` already exists.`};
        if(packge != undefined && packge.versions[pack.version] != undefined) return {ok: false, error: true, msg: `Package version \`V${pack.version}\` already present.`};
        if(!packge)
        {
            packge = {
                name: name,
                owner: user,
                versions: {},
                ts: 0
            };
            this.set(name, packge);
            packge = this.get(name);
        }
        packge.versions[pack.version] = pack;
        packge.ts = Date.now();
        return {ok:true, msg:`Package \`V${name}\` version \`N${pack.version}\` published`};
    }
    download(name, version){
        var packge = this.get(name);
        if(!packge) return {ok:false, error: true, msg:`Package \`V${name}\` not found`};
        if(version != undefined)
        {
            packge = packge.versions[version];
            if(!packge) return {ok: false, error: true, msg:`Package \`V${name} version \`N${version}\` not found.`};
        }
        else
        {
            packge = packge.versions[packge.latest_version];
        }
        return {ok:true, packge: packge, version: packge.version}
    }
}

module.exports.PackageManager = PackageManager;