Map.prototype.asArray = function(){
    let temp = Array.from(this);
    temp = temp.map(el=>{
        try {
            return el[1].toJObj();
        } catch(e) {
            return el[1];
        }
    });
    return temp;
}
Map.prototype.find = function(field, val){
    let temp = Array.from(this);
    var obj = null;
    temp.some(el=>{
        var key = el[0];
        var v = el[1];
        if(v[field] === val)
        {
            obj = v;
            return true;
        }
        else
        {
            return false;
        }
    });
    return obj;
}
global.storage = {};
global.storage.machines = new Map();
global.storage.users = new Map();
global.storage.gateways = new Map();
global.storage.dns = null;
global.storage.item_template = new Map();
global.storage.channels = null;
const fs = require('fs');
const config = JSON.parse(fs.readFileSync(process.cwd()+'/config.json').toString());
var inet = require('./modules/inet');
const Machine = require('./models/machine');
const Gateway = require('./models/gateway');
var db = require('./modules/database');
async function load(){
    console.log('Loading gateways...');
    var gateways = await db.con('gateways').find({}).toArray();
    gateways.forEach(gate=>{
        storage.gateways.set(gate.hwid, new Gateway(gate));
    });
    if(storage.gateways.get('vrealm-gateway-0000') == undefined)
    {
        console.log('No gateway present. Seeding...');
        storage.gateways.set('vrealm-gateway-0000', new Gateway({
            hwid: "vrealm-gateway-0000",
            dhcp: {
                block: "145.231",
                mapped: {},
                assigned: {},
                expires: {}
            }
        }));
    }
    console.log('Loading DNS...');
    var dns = await db.con('dns').find({id: 1}).toArray();
    storage.dns = dns.length > 0 ? inet.DNS.fromSave(dns[0]) : new inet.DNS(1);
    console.log('Loading machines...');
    var machines = await db.con('machines').find({}).toArray();
    machines.forEach(machine=>{
        var save = JSON.parse(JSON.stringify(machine));
        machined = new Machine(save);
        storage.machines.set(machined.hwid, machined);
    });
    console.log('Loading user data...');
    var users = await db.con('users').find({}).toArray();
    users.forEach(user=>{
        storage.users.set(user.uid, user);
    });
    console.log('Loading item templates...');
    var items = await db.con('item_template').find({}).toArray();
    items.forEach(item=>{
        storage.items.set(item.id, item);
    });
    console.log('Loading chat channels...');
    storage.channels = await db.con('channels').find({}).toArray();
    console.log('Done loading assets. Starting network...');
    storage.gateways.forEach(gate=>{
        gate.beginPoll();
    });
    storage.machines.forEach(machine=>{
        machine.beginTick();
    });
    if(config.live)
    {
        setInterval(()=>{
            //save
            var users = storage.users.asArray();
            var machines = storage.machines.asArray();
            var gateways = storage.gateways.asArray();
            var channels = storage.chat.channels.asArray();
            var dns = storage.dns.toJObj();
            users.forEach(user=>{
                db.con('users').updateOne({uid: user.uid},{$set: user}, {upsert: true});
            });
            machines.forEach(machine=>{
                db.con('machines').updateOne({hwid: machine.hwid},{$set: machine}, {upsert: true});
            });
            gateways.forEach(gate=>{
                db.con('gateways').updateOne({hwid: gate.hwid},{$set: gate}, {upsert: true});
            });
            channels.forEach(channel=>{
                db.con('channels').updateOne({name: channel.name},{$set: channel}, {upsert: true});
            });
            db.con('dns').updateOne({id: dns.id},{$set: dns}, {upsert: true});
        }, 2000);
    }
    require('./modules/network');
}
db.on('ready', ()=>{
    console.log("Database connection established");
    load();
});