var access_token = $("meta[name=access_token]").attr('content');
if(!access_token || access_token == 'false//#access_token;') window.location.href = 'https://vrealm.mudjs.net';
var tablist = [
    'container'
];
var colors = {
    '0':'#CACACA',
    '1':'#FFFFFF',
    '2':'#1EFF00',
    '3':'#0070DD',
    '4':'#B035EE',
    '5':'#FF8000',
    '6':'#FF8000',
    '7':'#FF8000',
    '8':'#FF8000',
    '9':'#FF8000',
    'a':'#000000',
    'b':'#3F3F3F',
    'c':'#676767',
    'd':'#7D0000',
    'e':'#8E3434',
    'f':'#A34F00',
    'g':'#725437',
    'h':'#A88600',
    'i':'#B2934A',
    'j':'#939500',
    'k':'#495225',
    'l':'#299400',
    'm':'#23381B',
    'n':'#00535B',
    'o':'#324A4C',
    'p':'#0073A6',
    'q':'#385A6C',
    'r':'#010067',
    's':'#507AA1',
    't':'#601C81',
    'u':'#43314C',
    'v':'#8C0069',
    'w':'#973984',
    'x':'#880024',
    'y':'#762E4A',
    'z':'#101215',
    'A':'#FFFFFF',
    'B':'#CACACA',
    'C':'#9B9B9B',
    'D':'#FF0000',
    'E':'#FF8383',
    'F':'#FF8000',
    'G':'#F3AA6F',
    'H':'#FBC803',
    'I':'#FFD863',
    'J':'#FFF404',
    'K':'#F3F998',
    'L':'#1EFF00',
    'M':'#B3FF9B',
    'N':'#00FFFF',
    'O':'#8FE6FF',
    'P':'#0070DD',
    'Q':'#A4E3FF',
    'R':'#0000FF',
    'S':'#7AB2F4',
    'T':'#B035EE',
    'U':'#E6C4FF',
    'V':'#FF00EC',
    'W':'#FF96E0',
    'X':'#FF0070',
    'Y':'#FF6A98',
    'Z':'#0C112B'
};

/// BEGIN UI
var styleDefault = {glow: 5, main: '#FFFFFF', input: '#FFFFFF', font_size:'10pt', background:'rgb(14,14,18)', font: 'VeraMono'}
var styleSetting = Object.assign({}, styleDefault);

function span(color, text){
    return `<span class="output_parsed" style="color: ${color}; text-shadow: 0 0 ${styleSetting.glow}px ${color};">${text}</span>`;
}

function color(...args){
    for(var i = 0;i<args.length;i++)
    {
        var arg = args[i];
        if(typeof(arg) == 'string')
        {
            var ic = arg.indexOf('`');
            while(ic != -1)
            {
                var color = colors[arg[ic+1]];
                var d = arg.indexOf('`', ic+1);
                var sub = arg.substring(ic+2, d);
                arg = arg.replace(arg.substring(ic, d+1), span(color, sub));
                ic = arg.indexOf('`');
            }
            args[i] = arg;
        }
    };
    return args.map(arg=>{
        return arg.toString();
    }).join('');
};

function setUI(){
    var objs = $('span.output_parsed');
    Array.from($('span.output_parsed')).forEach((d, i)=>{
        var el = $(objs[i]);
        if(el.css('text-shadow') != undefined)
        {
            var color = el.css('color');
            el.css('text-shadow', `0 0 ${styleSetting.glow}px ${color}`);
        }
    });
    return `@font-face{
        font-family: 'VeraMono';
        src: url('./terminal/fonts/VeraMono.ttf');
    }
    @font-face{
        font-family: 'ProggyClean';
        src: url('./terminal/fonts/ProggyClean.ttf');
    }
    ::selection {
    background: rgb(163, 171, 219);
}
html, body {
    text-rendering: geometricPrecision;
    width: 100%;
    height: 100%;
    margin: 0;
    overflow: auto;
    word-wrap: break-word;
    background-color: ${styleSetting.background};
}
.tab {
    overflow: hidden;
    height: 1.5em;
    padding: 0;
    border: 0px;
    background-color: rgba(100, 100, 100, 0.25);
}
.tab button {
    background-color: rgba(100, 100, 100, 0);
    font-family: 'VeraMono', monospace;
    font-size: 1.2em;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 0, 2px, 0, 2px;
    color: lightgray;
    transition: 0.3s;
}
/* Change background color of buttons on hover */
    .tab button:hover {
        background-color: rgba(100, 100, 100, 0.2);
    }

    .tab button.active:hover {
        color: white;
        text-shadow: 0 0 ${styleSetting.glow}px white;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        color: white;
        text-shadow: 0 0 ${styleSetting.glow}px white;
        background-color: rgba(100, 100, 100, 0.25);
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 0;
        margin: 0;
        border-top: none;
    }
body {
    font-size: ${styleSetting.font_size};
    font-family: '${styleSetting.font}', monospace;
    color: ${styleSetting.main};
    text-shadow: 0 0 ${styleSetting.glow}px ${styleSetting.main};
}
pre {
    text-rendering: geometricPrecision;
    font-family: '${styleSetting.font}', monospace;
    font-size: ${styleSetting.font_size};
}
#container {
    padding: .1em 1.5em 1em 1em;
}
#container #output {
    clear: both;
    width: 100%;
}
#container #output h3 {
    margin: 0;
}
#container #output pre {
    margin: 0;
}
.input-line {
    display: -webkit-box;
    -webkit-box-orient: horizontal;
    -webkit-box-align: stretch;
    display: -moz-box;
    -moz-box-orient: horizontal;
    -moz-box-align: stretch;
    display: box;
    box-orient: horizontal;
    box-align: stretch;
    clear: both;
}
.input-line > div:nth-child(2) {
    -webkit-box-flex: 1;
    -moz-box-flex: 1;
    box-flex: 1;
}
.prompt {
    white-space: nowrap;
    color: ${styleSetting.input};
    text-shadow: 0 0 ${styleSetting.glow}px ${styleSetting.input};
    margin-right: 7px;
    display: -webkit-box;
    -webkit-box-pack: center;
    -webkit-box-orient: vertical;
    display: -moz-box;
    -moz-box-pack: center;
    -moz-box-orient: vertical;
    display: box;
    box-pack: center;
    box-orient: vertical;
    -webkit-user-select: none;
    -moz-user-select: none;
    user-select: none;
}
.cmdline {
    outline: none;
    z-index: 1;
    background-color: transparent;
    margin: 0;
    width: 100%;
    font: inherit;
    border: none;
    overflow-y: auto;
    min-height: ${styleSetting.font_size};
    word-wrap:break-word;
    color: ${styleSetting.input};
    text-shadow: 0 0 ${styleSetting.glow}px ${styleSetting.input};
}
.cmdline2 {
    outline: none;
    z-index: 0;
    background-color: transparent;
    margin: 0;
    width: 100%;
    font: inherit;
    border: none;
    overflow-y: auto;
    min-height: ${styleSetting.font_size};
    word-wrap:break-word;
    color: #ff8000;
    text-shadow: 0 0 ${styleSetting.glow}px #ff8000;
    white-space: pre;
}
.ls-files {
    height: 45px;
    -webkit-column-width: 100px;
    -moz-column-width: 100px;
    -o-column-width: 100px;
    column-width: 100px;
}`
};

function setStyle(options = styleSetting){
    if(options.default === true)
    {
        styleSetting = Object.assign({}, styleDefault);
    }
    else
    {
        options = Object.assign({}, styleSetting, options);
        styleSetting = options;
    }
    $('style#terminal').html(setUI());
}

setStyle();

/// END UI

function openTab(evt, name) {
    // Declare all variables
    var i, tabcontent, tablinks;
    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(name).style.display = "block";
    evt.currentTarget.className += " active";
}

var tabs = new Map();

function createTab(id, title, content, close = function(){}, open = true){
    var tab = $('.tab');
    var tbtn = $(`<div id="${id}-tab"><button id="${id}-btn" class="tablinks">${title}</button><button id="${id}-btn-close" class="tabclose">X</button></div>`);
    var tctn = $(`<div id="${id}" class="tabcontent" style="max-width: 100%; max-height: 96%; overflow: hidden; border: 0px; height: 100%; width: 100%;"></div>`);
    tctn.html(content);
    tab.append(tbtn);
    $('body').append(tctn);
    tablist.push(id);
    tabs.set(id, {onclose: close});
    $(`#${id}-btn`).click(function(e){
        openTab(e, `${id}`)
    });
    $(`#${id}-btn-close`).click(function(e){
        closeTab(e, `${id}`)
    });
    if(open) $(`#${id}-btn`).click();
}

function closeTab(event, id){
    var tab = tabs.get(id);
    if(tab.onclose) tab.onclose();
    var i = tablist.findIndex(el=>{
        return el == id;
    });
    if(tablist.length > 1)
    {
        document.getElementById(tablist[i-1]+'-btn').click();
        if(i != -1) tablist.splice(i, 1);
    }
    else
    {
        document.getElementById(tablist[0]+'-btn').click();
    }
    $(`#${id}-tab`).remove();
    $(`#${id}`).remove();
}

$('#container-btn').click((e)=>{
    openTab(e, 'container');
});

$('#container-btn').click();

var ws = null;

var terminaldefaults = {colors: color, prompt:'$'};
var terminal;
var user = '';

class Terminal {
    constructor(id, options = terminaldefaults){
        options = Object.assign({}, terminaldefaults, options);
        this.target = $('#'+id);
        this.output = $('<pre id="output"></pre>');
        this.input = $('<input type="text" id="terminal-input" class="cmdline" autofocus />');
        this.options = options;
        this.colors = this.options.colors;
        this.prompt_text = this.options.prompt;
        this.prompt = $('<div class="prompt"></div>');
        this.postprocess = new Map();
        this.preprocess = new Map();
        this.ready = false;
        this.history = [];
        this.histpos = this.history.length-1;
        this.histtemp = 0;
        this.locked = true;
        this.canLog = false;
        this.canInput = false;
        this.prompt_id = null;
        this.deferred = null;
    }
    clear(){
        const self = this;
        this.output.empty();
        this.history = [];
        shell = [];
        this.histpos = 0;
    }
    setPrompt(text){
        this.prompt_text = text;
        this.prompt.html(text);
    }
    init(){
        const self = this;
        this.prompt.html(this.prompt_text);
        this.target.append(this.output);
        var inputline = $('<div class="input-line"></div>');
        inputline.append(this.prompt);
        var incont = $('<div></div>');
        incont.append(this.input);
        inputline.append(incont);
        this.target.append(inputline);
        window.addEventListener('click', function(e) {
            self.input.focus();
        }, false);

        // COMMIT/AUTOs
        this.input.keydown(e=>{
            if(!this.canInput) e.preventDefault();
            if(this.deferred != null)
            {
                if(e.keyCode == 8)
                {
                    e.preventDefault();
                    if(this.deferred != '')
                        this.deferred = this.deferred.substring(0, this.deferred.length-1);
                }
                else if(e.keyCode == 13 && !e.altKey){
                    e.preventDefault();
                    if(this.deferred == '') return;
                    self.log(self.prompt_text);
                    self.processCommand(this.deferred);
                    this.deferred = null;
                }
                else if(e.keyCode == 27){
                    e.preventDefault();
                    this.deferred = '';
                }
                else
                {
                    e.preventDefault();
                    this.deferred += String.fromCharCode($.getChar(e));
                }
            }
            else
            {
                const self = this;
                var histpos_ = this.histpos;
                var input = self.input;
                var history_ = self.history;
                if (e.keyCode == 9) { // tab
                    input.focus();
                    e.preventDefault();
                } else if (e.keyCode == 13 && !e.altKey) { // enter
                    // Save shell history.
                    if (input.val()) {
                        if(this.prompt_id == null)
                        {
                            self.histpos = self.history.push(input.val());
                        }
                        self.log(self.prompt_text+' '+input.val());
                        self.processCommand(input.val());
                        input.val('');
                    }
                } else if (e.keyCode == 27) {//escape
                    input.val('');
                }
            }
        });

        // HISTORY
        this.input.keydown(e=>{
            var history_ = this.history;
            var input = this.input;
            if (this.history.length) {
                if (e.keyCode == 38 || e.keyCode == 40) {
                    if (this.history[this.histpos]) {
                    this.history[this.histpos] = input.val();
                    } else {
                    this.histtemp = input.val();
                    }
                }
            
                if (e.keyCode == 38) { // up
                    this.histpos--;
                    if (this.histpos < 0) {
                    this.histpos = 0;
                    }
                } else if (e.keyCode == 40) { // down
                    this.histpos++;
                    if (this.histpos > this.history.length) {
                    this.histpos = this.history.length;
                    }
                }
        
                if (e.keyCode == 38 || e.keyCode == 40) {
                    input.val(this.history[this.histpos] ? this.history[this.histpos] : this.histtemp);
                    input.focus();
                    var len = input.val().length;
                    setTimeout(function() {
                        input[0].setSelectionRange(len, len);
                    }, 0);
                }
            }
        });
        this.input.click(e=>{
            self.input.val(self.input.val());
        });
        this.ready = true;
        this.locked = false;
        this.canInput = true;
        this.canLog = true;
    }
    log(text, to_shell = true, post = true){
        if(!this.canLog) return;
        if(!this.ready) throw new Error('Terminal not ready.');
        if(post)
        {
            if(this.preprocess.size > 0)
            {
                this.preprocess.forEach(proc=>{
                    text = proc(text);
                });
            }
        }
        text = this.colors(text);
        if(post)
        {
            if(this.postprocess.size > 0)
            {
                this.postprocess.forEach(proc=>{
                    text = proc(text);
                });
            }
        }
        this.output.append(`<p>${text}</p>`);
        $('#container').scrollTop($('#container')[0].scrollHeight - $('#container')[0].clientHeight);
    }
    logAnimateCreate(id){
        if(!this.canLog) return;
        if(!this.ready) throw new Error('Terminal not ready.');
        var field = $(`<p id="run_id_${id}"></p>`);
        this.output.append(field);
        $('#container').scrollTop($('#container')[0].scrollHeight - $('#container')[0].clientHeight);
    }
    logAnimateUpdate(val, id){
        if(!this.canLog) return;
        if(!this.ready) throw new Error('Terminal not ready.');
        var field = $('#run_id_'+id);
        if(field){
            field.html(this.colors(val));
            $('#container').scrollTop($('#container')[0].scrollHeight - $('#container')[0].clientHeight);
        }
    }
    logLast(val){
        if(!this.ready) throw new Error('Terminal not ready.');
        $('#output:last-child').html(val);
        $('#container').scrollTop($('#container')[0].scrollHeight - $('#container')[0].clientHeight);
    }
    applyPostProcess(name, middleware = function(input){return input;}){
        this.postprocess.set(name, middleware);
    }
    applyPreProcess(name, middleware = function(input){return input;}){
        this.preprocess.set(name, middleware);
    }
    processCommand(raw){
        raw = raw.trim();
        if(raw[0] == "~")
        {
            raw = raw.substring(1);
            var cmd = raw.split(' ')[0].toLowerCase();
            var args = raw.split(' ').slice(1).join(' ');
            if(!raw.split(' ')[1]) args = undefined;
            switch(cmd){
                case 'gui':
                    if(args == '' || args == undefined)
                    {
                        this.log('GUI Settings '+JSON.stringify(styleSetting));
                    }
                    else
                    {
                        console.log(args);
                        args = args.toLowerCase();
                        try {
                            args = new Function(`return (${args})`)();
                            setStyle(args);
                            this.log('`2GUI settings updated.`');
                        } catch(e){
                            this.log('`DError`: '+e.message);
                        }
                    }
                break;
                default:
                    this.log('`DError`: `V'+cmd+'` is not a valid command.');
                break;
            }
        }
        else
        {
            if(ws != null)
            {
                if(this.prompt_id == null)
                    ws.send(JSON.stringify({op:'RUN', command: raw}));
                else
                    ws.send(JSON.stringify({op:'PROMPT', id: this.prompt_id, val: raw}));
                this.canInput = false;
            }
        }
    }
}

terminal = new Terminal('container');
terminal.init();

function handleData(dobj){
    if(dobj.data != undefined && dobj.data.retval != undefined)
    {
        var ret = dobj.data.retval;
        var msg = '';
        if(typeof ret == 'object' && ret.ok != undefined && ret.msg != undefined)
        {
            msg += (ret.ok?'`2Success`':'`DFailure`');
            msg += '\n'+ret.msg;
        }
        else if(typeof ret == 'object' && ret.ok != undefined)
        {
            msg = (ret.ok?'`2Success`':'`DFailure`');
        }
        else if(typeof ret == 'object' && ret.msg != undefined)
        {
            msg = ret.msg;
        }
        else
        {
            if(typeof(ret) == "object" && !(ret instanceof Array))
            {
                msg = JSON.stringify(ret, null, '  ');
            }
            else if(ret instanceof Array)
            {
                msg = ret.map(el=>{
                    switch(typeof el){
                        case 'object':
                            return JSON.stringify(el);
                        break;
                        default:
                            return el.toString();
                        break;
                    }
                }).join('\n');
            }
            else if(ret == null) msg = '\n';
            else msg = ret.toString();
        }
        terminal.log(msg);
    }
    else if(dobj.msg != undefined)
    {
        terminal.log(dobj.msg);
    }
}

function connect(){
    ws = new WebSocket(`wss://vrealm.mudjs.net/ws`);
    ws.addEventListener('open', ev=>{
        terminal.log('`2Connected`');
        ws.send(JSON.stringify({op:"AUTHENTICATE", key: access_token, web: true}));
    });
    ws.addEventListener('error', ev=>{

    });
    ws.addEventListener('close', ev=>{
        if(ws != null)
        {
            terminal.log('`DLost connection to the server`');
            ws = null;
            Array.from(tabs).forEach(el=>{
                closeTab(null, el[0]);
            });
        }
        setTimeout(()=>{
            connect();
        }, 5000);
    });

    ws.addEventListener('message', ev=>{
        var data = ev.data;
        try {
            var dobj = JSON.parse(data);
            //console.log(dobj);
            if(dobj.op != undefined)
            {
                switch(dobj.op)
                {
                    case 'RUN':
                        if(dobj.val != undefined) handleData(dobj.val);
                        terminal.canInput = true;
                    break;
                    case 'PROMPT':
                        terminal.setPrompt(color(dobj.prompt));
                        if(dobj.input != undefined)
                        {
                            if(dobj.input.id != undefined)
                            {
                                if(dobj.input.password === true)
                                    terminal.deferred = '';
                                terminal.prompt_id = dobj.input.id;
                            }
                            else
                            {
                                terminal.deferred = null;
                                terminal.prompt_id = null;
                            }
                        }
                    break;
                    case 'BEGINSTDOUT':
                        terminal.log('');
                        terminal.canLog = false;
                        terminal.canInput = false;
                    break;
                    case 'DOWNLOAD':
                        window.open('/download/'+dobj.uuid+'/'+dobj.id, '_blank');
                    break;
                    case 'STDOUT':
                        terminal.logLast(dobj.val);
                    break;
                    case 'ENDSTDOUT':
                        terminal.canLog = true;
                        terminal.canInput = true;
                    break;
                    case 'TEXTEDIT':
                        createTab(`nano-${dobj.fid}`, dobj.path, `<iframe src="/editor?uuid=${dobj.uuid}&fid=${dobj.fid}&title=${dobj.path}" style="border: 0px; display: block; width: 100%; height: 100%; margin: 0;"></iframe>`,
                        function(){
                            $.post('/api/v1/filesystem/close', {uuid: dobj.uuid, fid: dobj.fid, path: dobj.path});
                        }, true);
                    break;
                    case 'REALTIME':
                        if(dobj.create === true)
                        {
                            console.log("Creating realtime script for id:",dobj.run_id);
                            terminal.logAnimateCreate(dobj.run_id);
                        }
                        else
                        {
                            terminal.logAnimateUpdate(JSON.parse(dobj.val), dobj.run_id);
                        }
                    break;
                }
            }
            else
                handleData(dobj);
        } catch(e){
            console.log(e);
        }
    });
}

connect();