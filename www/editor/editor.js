function getAllUrlParams(url) {
    // get query string from url (optional) or window
    var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

    // we'll store the parameters here
    var obj = {};

    // if query string exists
    if (queryString) {

    // stuff after # is not part of query string, so get rid of it
    queryString = queryString.split('#')[0];

    // split our query string into its component parts
    var arr = queryString.split('&');

    for (var i=0; i<arr.length; i++) {
        // separate the keys and the values
        var a = arr[i].split('=');

        // in case params look like: list[]=thing1&list[]=thing2
        var paramNum = undefined;
        var paramName = a[0].replace(/\[\d*\]/, function(v) {
        paramNum = v.slice(1,-1);
        return '';
        });

        // set parameter value (use 'true' if empty)
        var paramValue = typeof(a[1])==='undefined' ? true : a[1];

        // (optional) keep case consistent
        paramName = paramName.toLowerCase();
        paramValue = decodeURIComponent(paramValue);
        // if parameter name already exists
        if (obj[paramName]) {
        // convert value to array (if still string)
        if (typeof obj[paramName] === 'string') {
            obj[paramName] = [obj[paramName]];
        }
        // if no array index number specified...
        if (typeof paramNum === 'undefined') {
            // put the value on the end of the array
            obj[paramName].push(paramValue);
        }
        // if array index number specified...
        else {
            // put the value at that index number
            obj[paramName][paramNum] = paramValue;
        }
        }
        // if param name doesn't exist yet, set it
        else {
            obj[paramName] = paramValue;
        }
    }
    }
    return obj;
}
var params = getAllUrlParams();
var uuid = params.uuid;
var fid = params.fid;
var data = $("#editor-data").val();
$("#editor-data").remove();
var title = params.title ? params.title : 'Editor';
document.title = title;
$('#editoruuid').val(uuid);
$('#editorfid').val(fid);
$('#editorpath').val(title);
var editor = ace.edit(document.getElementById("editor"));
editor.setTheme("ace/theme/monokai");
editor.session.setMode('ace/mode/javascript');
editor.setValue(data, -1);
var saving = false;
function saveFile(){
    if(saving === true) return;
    saving = true;
    var edata = editor.getValue();
    $('.SaveFile').addClass('saved');
    $.ajax({
        url: '/api/v1/filesystem/upload',
        contentType: 'application/json',
        data: JSON.stringify({
            uuid: uuid,
            fid: fid,
            path: title,
            data: edata
        }),
        method: 'POST',
        complete: function(){
            data = edata;
            $('.SaveFile.saved').removeClass('saved');
            saving = false;
        }
    });
}
window.addEventListener("keydown", function (e) {
    if((e.ctrlKey && e.keyCode == 83) || (e.metaKey && e.keyCode == 83)){
        e.preventDefault();
        saveFile();
    }
});
$("#fileupload").change((e)=>{
    var file = $("#fileupload").prop('files')[0];
    var reader = new FileReader();
    reader.onloadend = function(e){
        editor.setValue(reader.result, -1);
    }
    if(file.type.includes('image'))
        reader.readAsDataURL(file);
    else
        reader.readAsText(file);
});
$("#save-btn").click((e)=>{
    saveFile();
});
setInterval(()=>{
    let temp = editor.getValue();
    if(temp != data && document.title != title+' (*)')
    {
        document.title = title+' (*)';
    }
    else if(temp == data && document.title != title)
    {
        document.title = title;
    }
}, 100);