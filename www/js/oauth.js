var queryString = window.location.hash.slice(1);
var params = new URLSearchParams(queryString);
if(!params.get('access_token') || !params.get('state')) window.location.href('https://vrealm.mudjs.net');
var accessToken = params.get("access_token");
var nonce = params.get("state");
window.location.href = `https://vrealm.mudjs.net/api/v1/oauth?access_token=${accessToken}&n=${nonce}`;