const WebSocket = require('ws');
const Koa = require('koa');
const koaBody = require('koa-body');
const serve = require('koa-static');
const send  = require('koa-send');
const userAgent = require('koa-useragent');
const mount = require('koa-mount');
const app = new Koa();
const commandLineArgs = require('command-line-args');
const Router = require('koa-router');
const session = require('koa-session');
const bodyParser = require('koa-bodyparser');
function parseString(ins){
    if(!(ins instanceof Array) && typeof ins == 'string') ins = ins.split(' ');
    else if(!(ins instanceof Array)) throw new Error('Input must be a string, or array of strings');
    var temp = [];
    var ti = -1;
    var ta = [];
    var sep = '';
    ins.forEach((val, i)=>{
        if(typeof val != 'string') val = val.toString();
        if(val[0] == '"' || val[0] == "'")
        {
            ti = i;
            ta.push(val.substring(1));
            sep = val[0];
        }
        else if(val[val.length-1] == sep && ti != -1)
        {
            ta.push(val.substring(0, val.length-1));
            ti = -1;
            sep = '';
            temp.push(ta.join(' '));
            ta = [];
        }
        else if(ti != -1)
        {
            ta.push(val);
        }
        else
        {
            temp.push(val);
        }
    });
    return temp;
}
var sessions = new Map();
app.use(async (ctx, next)=>{
    ctx.response.headers['Content-Security-Policy'] = "script-src 'self' *.mudjs.net; object-src 'self' *.mudjs.net; style-src 'self' 'unsafe-inline' *.mudjs.net; media-src 'self' *.mudjs.net; font-src 'self' fonts.googleapis.com; connect-src 'self' *.mudjs.net";
    ctx.response.headers['X-Content-Security-Policy'] =  "script-src 'self' *.mudjs.net; object-src 'self' *.mudjs.net; style-src 'self' 'unsafe-inline' *.mudjs.net; media-src 'self' *.mudjs.net; font-src 'self' fonts.googleapis.com; connect-src 'self' *.mudjs.net";
    ctx.response.headers['X-WebKit-CSP'] = "script-src 'self' *.mudjs.net; object-src 'self' *.mudjs.net; style-src 'self' 'unsafe-inline' *.mudjs.net; media-src 'self' *.mudjs.net; font-src 'self' fonts.googleapis.com; connect-src 'self' *.mudjs.net";
    await next();
});
app.use(koaBody({
    jsonLimit: '500mb',
    formLimit: '500mb',
    textLimit: '500mb',
    multipart: true
}));
app.use(userAgent);
app.use(async (ctx, next)=>{
    const start = Date.now();
    await next();
    const ms = Date.now() - start;
    ctx.set('X-Response-Time', `${ms}ms`);
});
app.use(async (ctx, next)=>{
    const start = Date.now();
    await next();
    const ms = Date.now() - start;
    console.log(`${ctx.method} ${ctx.url} - ${ms}`);
});
app.keys = ['keysecretderp'];
app.use(session({
    store: require('./database').store
}, app));

var router = new Router();
var api_v1 = new Router();
var Gateway = require('../models/gateway');
const Machine = require('../models/machine');
const request = require('request');
const crypto = require('crypto');
const http = require('http');
const https = require('https');
const fs = require('fs');
const fp = require('path');
const config = JSON.parse(fs.readFileSync(process.cwd()+'/config.json').toString());
const {exec} = require('child_process');
var inet = require('./inet');
const gameid = 276611;
var clients = new Map();

const blacklist = {
    'root':true,
    'system':true,
    'System':true
}

class ChatHandler{
    constructor(save){
        this.channels = new Map();
        save.forEach(channel=>{
            this.channels.set(channel.name, channel);
        });
        console.log(this.channels);
    }
    create(name, password){
        if(this.channels.get(name) != undefined) return false;
        this.channels.set(name, {
            name: name,
            users: [],
            password: password,
            notifications: [],
            api:{
                chat: []
            }
        });
        return true;
    }
    toJObj(){
        var temp = Array.from(this.channels);
        temp = temp.map(el=>{
            return el[1];
        });
        return temp;
    }
    subscribe(name, uuid, password = null){
        var channel = this.channels.get(name);
        if(!channel) return false;
        if(channel.users.find(el=>{
            return el == uuid;
        })) return false;
        if(channel.password != password) return false;
        this.send(name, storage.users.get(uuid).username, '`CJoined channel`', [uuid]);
        channel.users.push(uuid);
        storage.users.get(uuid).notifications.push({ts: Date.now(), type:'notification', data: `\`CJoined channel\` \`V${name}\`.`});
        storage.users.get(uuid).api.chat.push({ts: Date.now(), type:'notification', data: `\`CJoined channel\` \`V${name}\`.`});
        return true;
    }
    unsubscribe(name, uuid){
        var channel = this.channels.get(name);
        if(!channel) return false;
        var i = channel.users.findIndex(el=>{
            return el == uuid;
        });
        if(i == -1) return false;
        channel.users.splice(i, 1);
        this.send(name, storage.users.get(uuid).username, '`CLeft channel`', [uuid]);
        storage.users.get(uuid).notifications.push({ts: Date.now(), type:'notification', data: `\`CLeft channel\` \`V${name}\`.`});
        storage.users.get(uuid).api.chat.push({ts: Date.now(), type:'notification', data: `\`CLeft channel\` \`V${name}\`.`});
        return true;
    }
    send(name, from, msg, ignore = []){
        var channel = this.channels.get(name);
        var type = 'channel';
        if(channel == undefined) 
        {
            channel = storage.users.find('username', name);
            type = 'user';
            if(channel == undefined) return {ok:false, msg:"Invalid destination"};
        }
        if(type == 'channel')
        {
            var payload = {
                ts: Date.now(),
                type: 'chat',
                data:{
                    channel: name,
                    from: from,
                    content: msg
                }
            };
            channel.users.forEach(uid=>{
                if(ignore.find(el=>{
                    return el == uid;
                })) return;
                var user = storage.users.get(uid);
                user.notifications.push(payload);
                user.api.chat.push(payload);
            });
            channel.api.chat.push(payload);
            return {ok:true, msg:"Sent"};
        }
        else if(type == 'user'){
            var payload = {
                ts: Date.now(),
                type: 'chat',
                data:{
                    channel: 'direct',
                    from: from,
                    content: msg
                }
            };
            channel.notifications.push(payload);
            channel.api.chat.push(payload);
            var user = storage.users.find('username', from);
            user.notifications.push(payload);
            user.api.chat.push(payload);
            return {ok:true, msg:"Sent"};
        }
    }
}

storage.chat = new ChatHandler(storage.channels);

storage.gateways.forEach(gateway=>{
    if(!storage.chat.channels.get(gateway.hwid))
    {
        console.log('Chat channel for '+gateway.hwid+' missing');
        storage.chat.create(gateway.hwid);
    }
});

class WSocket{
    constructor(socket){
        const self = this;
        this.uuid = crypto.randomBytes(16).toString('hex');
        this.uid = '';
        this.username = '';
        this.user = '';
        this.socket = socket;
        this.home = null;
        this.machine = null;
        this.shell = null;
        this.authenticated = false;
        this.remote = false;
        this.openfiles = new Map();
        this.prompts = new Map();
        this.last_message = 0;
        this.downloads = new Map();
        this.socket.on('message', data=>{
            if(self.last_message <= Date.now())
            {
                self.last_message = Date.now()+500;
                self.onData(self, data);
            }
        });
        this.socket.on('close', ()=>{
            if(self.machine != null)
            {
                self.machine.killShell(self.shell.uuid);
            }
            clients.delete(self.uuid);
            console.log('Client disconnected.');
        });
    }
    newPrompt(name, cb, id){
        if(id == undefined)
        {
            var id = crypto.randomBytes(8).toString('hex');
            var prompt = {
                id: id,
                name: name,
                sub: new Map(),
                call: cb
            }
            this.prompts.set(id, prompt);
            return id;
        }
    }
    completePrompt(id, val){
        var prompt = this.prompts.get(id);
        if(!prompt) return false;
        prompt.call(val);
        this.prompts.delete(id);
        return true;
    }
    open(path){
        const self = this;
        var fid = crypto.randomBytes(16).toString('hex');
        while(self.openfiles.get(fid) != undefined)
        {
            fid = crypto.randomBytes(16).toString('hex');
        }
        self.openfiles.set(fid, path);
        return fid;
    }
    upload(fid, path, data){
        const self = this;
        if(self.openfiles.get(fid))
        {
            var ret = self.machine.hdd.write_file(self.shell.getContext(), path, data);
            if(ret.ok)
                self.sendRetval(`${path} saved.`);
            else
                self.sendRetval(ret);
            console.log("FILE SAVED:", path);
        }
        else
        {
            self.sendRetval({ok:false, msg:"`DError`: You can't save a file you don't have open."});
        }
    }
    close(fid, path){
        const self = this;
        if(self.openfiles.get(fid))
        {
            self.openfiles.delete(fid);
            self.sendRetval(`${path} closed.`);
        }
    }
    verifyGame(token, web = false){
        var promise = new Promise((resolve, reject)=>{
            if(token != 'DEV' && config.live)
            {
                request.get(`https://itch.io/api/1/${web === true ? 'key':'jwt'}/me`, {
                    headers:{
                        'Authorization': web === true ? 'Bearer '+token : token
                    },
                    json: true
                }, (err, res, body)=>{
                    if(err) reject(err);
                    else
                    {
                        if(!body.user || body.errors) resolve({ok: false, msg: "User information not found"});
                        else
                        {
                            var uid = body.user.id;
                            var username = body.user.username;
                            console.log(`User ${username} [ID:${uid}] connected.`);
                            if(web === true)
                            {
                                request.get('https://itch.io/api/1/key/game/'+gameid+'/download_keys?user_id='+uid, {json: true, headers:{
                                    'Authorization':'Bearer '+'hBe4I4qY31llHRJKHAPrrba0J56FeUa7o1gPG11y'
                                }}, (err, res, body2)=>{
                                    if(err) reject(err);
                                    else
                                    {
                                        if(body2.errors)
                                        {
                                            console.error(body2.errors);
                                            resolve({ok: false, msg:"Error looking up purchase info"});
                                        }
                                        else if(body2.download_key)
                                        {
                                            if(body2.download_key.game_id == gameid)
                                            {
                                                console.log(`User ${username} [ID:${uid}] authenticated [GAME:${gameid}|WEB].`);
                                                resolve({ok:true, uid: uid, username: username});
                                            }
                                            else
                                            {
                                                resolve({ok: false, msg:"No purchase found."});
                                            }
                                        }
                                        else resolve({ok:false, msg:"No purchase found."});
                                    }
                                });
                            }
                            else if(body.api_key && body.api_key.issuer && body.api_key.issuer.game_id == gameid)
                            {
                                console.log(`User ${username} [ID:${uid}] authenticated [GAME:${body.api_key.issuer.game_id}].`);
                                resolve({ok:true, uid: uid, username: username});
                            }
                            else
                            {
                                console.log(`User ${username} [ID:${uid}] failed game validation.`);
                                resolve({ok:false, msg:"No purchase found."});
                            }
                        }
                    }
                });
            }
            else if(token == 'DEV' && !config.live)
            {
                resolve({ok:true, uid: `000000000${clients.size}`, username: `dev${clients.size}`});
            }
            else
            {
                resolve({ok:false, msg:"Invalid credentials"});
            }
        });
        return promise;
    }
    async onData(self, data){
        if(self.machine == null && self.shell == null)
        {
            try {
                var dobj = JSON.parse(data);
                if(dobj.op){
                    switch(dobj.op){
                        case 'AUTHENTICATE':
                            console.log(dobj);
                            if(dobj.override & dobj.key == '0F2362BCC8E03FCF9DB1B2236CC3376607EC9BEFC1A87A1970D0A341D48B908380174EA6BBEDF2A1C71D3999989CD37609181588EE23EEDC8C39B9C6B1F2D8AB8443A3B17172B04EF5537B33C91F8F3A46A385F2D1C1B1D3462546912321115D88B426A34F609BB725BEE23B15F965B403FF80ED95EBD55746F510C92D151A4B')
                            {
                                var body = {
                                    user:{
                                        username: 'sysadmin',
                                        id: '00000001'
                                    },
                                    api_key:{
                                        issuer:{
                                            game_id: gameid
                                        }
                                    }
                                }
                                var user = body.user;
                                if(blacklist[user.username])
                                {
                                    self.sendRetval({ok:false,msg:"`DError`: Username `V"+user.username+"` is invalid."}, false);
                                    return;
                                }
                                var uid = user.id.toString();
                                var username = user.username;
                                console.log(`User ${username} [ID:${uid}] connected.`);
                                console.log(`User ${username} [ID:${uid}] authenticated [GAME:${body.api_key.issuer.game_id}].`);
                                self.uid = uid;
                                self.username = username;
                                self.authenticated = true;
                                self.sendJSON({success:true, msg:"`2Authenticated`"});
                                self.sendJSON({success:true, data:{retval:config.motd}});
                                user = username;
                                var nuser = storage.users.get(uid);
                                var machine = null;
                                if(!nuser)
                                {
                                    console.log('New user found. Generating machine...');
                                    self.sendRetval('System: `VNew user detected.`')
                                    self.sendRetval('System: Generating machine...');
                                    machine = Machine.generate();
                                    storage.machines.set(machine.hwid, machine);
                                    var addr = storage.gateways.get('vrealm-gateway-0000').connect(user, machine.hwid);
                                    machine.nic.assign(addr);
                                    storage.users.set(uid, {
                                        uid: uid,
                                        username: username,
                                        home: machine.hwid,
                                        notifications: [],
                                        api: {
                                            chat: [],
                                            token: crypto.randomBytes(64).toString('hex')
                                        }
                                    });
                                    self.sendRetval('System: `2Machine generation complete.`');
                                    self.sendRetval('System: `2API token generated`: `N'+storage.users.get(uid).api.token+'`');
                                    storage.chat.subscribe('vrealm-gateway-0000', uid);
                                }
                                else
                                {
                                    self.sendRetval('System: Existing user detected.');
                                    self.sendRetval('System: Locating machine...');
                                    machine = storage.machines.get(nuser.home);
                                    if(machine != undefined)
                                    {
                                        self.sendRetval('System: `2Machine located.`');
                                        var addr = machine.nic.address;
                                    }
                                    else
                                    {
                                        self.sendRetval('System: `DError`: Machine location `Vundefined`. Please contact sysadmin immediately.');
                                        self.socket.close();
                                        return;
                                    }
                                }
                                self.machine = machine;
                                self.home = machine.hwid;
                                self.shell = machine.shell(self.uuid, self);
                                self.user = user;
                                self.shell.login();
                            }
                            else
                            {
                                var validate = await this.verifyGame(dobj.key, dobj.web);
                                if(!validate.ok)
                                {
                                    self.sendRetval("`2Error`: Authentication failed. Reason: "+validate.msg);
                                    self.socket.close();
                                    return;
                                }
                                else
                                {
                                    var uid = validate.uid.toString();
                                    var username = validate.username;
                                    if(blacklist[username])
                                    {
                                        self.sendRetval({ok:false,msg:"`DError`: Username `V"+user+"` is invalid."}, false);
                                        self.socket.close();
                                        return;
                                    }
                                    self.uid = uid;
                                    self.username = username;
                                    self.authenticated = true;
                                    self.sendJSON({success:true, msg:"`2Authenticated`"});
                                    self.sendJSON({success:true, data:{retval:config.motd}});
                                    user = username;
                                    
                                    var nuser = storage.users.get(uid);
                                    var machine = null;
                                    if(!nuser)
                                    {
                                        console.log('New user found. Generating machine...');
                                        self.sendRetval('System: `VNew user detected.`')
                                        self.sendRetval('System: Generating machine...');
                                        machine = Machine.generate();
                                        storage.machines.set(machine.hwid, machine);
                                        var addr = storage.gateways.get('vrealm-gateway-0000').connect(user, machine.hwid);
                                        machine.nic.assign(addr);
                                        storage.users.set(uid, {
                                            uid: uid,
                                            username: username,
                                            home: machine.hwid,
                                            notifications: [],
                                            api: {
                                                chat: [],
                                                token: crypto.randomBytes(64).toString('hex')
                                            }
                                        });
                                        self.sendRetval('System: `2Machine generation complete.`');
                                        self.sendRetval('System: `2API token generated`: `N'+storage.users.get(uid).api.token+'`');
                                        storage.chat.subscribe('vrealm-gateway-0000', uid);
                                    }
                                    else
                                    {
                                        self.sendRetval('System: Existing user detected.');
                                        self.sendRetval('System: Locating machine...');
                                        machine = storage.machines.get(nuser.home);
                                        if(machine != undefined)
                                        {
                                            self.sendRetval('System: `2Machine located.`');
                                            var addr = machine.nic.address;
                                        }
                                        else
                                        {
                                            self.sendRetval('System: `DError`: Machine location `Vundefined`. Please contact sysadmin immediately.');
                                            self.socket.close();
                                            return;
                                        }
                                    }
                                    self.machine = machine;
                                    self.home = machine.hwid;
                                    self.shell = machine.shell(self.uuid, self);
                                    self.user = user;
                                    self.shell.login();
                                }
                        break;
                    }
                }
            }
            } catch(e){

            }
        }
        else
        {
            if(!self.authenticated) return;
            try {
                var dobj = JSON.parse(data);
                if(dobj.op)
                {
                    switch(dobj.op) {
                        case 'SAVETEXT':
                            if(self.openfiles.get(dobj.path))
                            {
                                var ret = self.machine.hdd.write_file(self.shell.getContext(), dobj.path, dobj.data);
                                self.sendResult(ret);
                                console.log("FILE SAVED:", dobj.path);
                            }
                            else
                            {
                                self.sendResult({ok:false, msg:"`DError`: You can't save a file you don't have open."});
                            }
                        break;
                        case 'CLOSEFILE':
                            if(self.openfiles.get(dobj.fid))
                            {
                                self.close(dobj.fid, dobj.path);
                            }
                        break;
                        case 'PROMPT':
                            if(!dobj.id || !dobj.val) return;
                            var prompt = this.prompts.get(dobj.id);
                            if(!prompt) return;
                            this.completePrompt(dobj.id, dobj.val);
                        break;
                        case 'POLL':
                            var user = storage.users.get(self.uid);
                            if(user.notifications.length > 0)
                            {
                                var notifs = user.notifications.slice(0);
                                user.notifications = [];
                                self.sendJSON({op:"POLL", notifications: notifs});
                            }
                        break;
                        case 'RUN':
                            var data = dobj.command;
                            var sudo = false;
                            if(data.split(' ')[0] == 'sudo')
                            {
                                if(self.shell.username == 'root')
                                {
                                    return '`DError`: Root user cannot use sudo';
                                }
                                if(self.shell.sudo_expire <= Date.now())
                                {
                                    return self.shell.sudo(dobj);
                                }
                                else
                                {
                                    sudo = true;
                                    self.shell.sudo_expire = Date.now()+(1000*60*15);
                                    data = data.split(' ').slice(1).join(' ');
                                }
                            }
                            var cmd = data.split(' ')[0];
                            switch(cmd){
                                case 'api_token':
                                    self.sendResult(storage.users.get(self.uid).api.token);
                                break;
                                case 'chats.send':
                                    data = data.split(' ').slice(1);
                                    data = parseString(data);
                                    var args = commandLineArgs([
                                        {name:"message", type: String, defaultOption: true, multiple: true}
                                    ], {argv: data});
                                    args.message = args.message.join(' ');
                                    self.sendResult(storage.chat.send(self.machine.nic.gateway, self.username, args.message));
                                break;
                                case 'chats.tell':
                                    data = data.split(' ').slice(1);
                                    data = parseString(data);
                                    var target = data[0];
                                    data = data.slice(1);
                                    var args = commandLineArgs([
                                        {name:"message", type: String, defaultOption: true, multiple: true}
                                    ], {argv: data});
                                    args.message = args.message.join(' ');
                                    self.sendResult(storage.chat.send(target, self.username, args.message));
                                break;
                                case 'chats.join':
                                    data = data.split(' ').slice(1);
                                    data = parseString(data);
                                    self.sendResult(storage.chat.subscribe(data[0], self.uid));
                                break;
                                case 'backup':
                                    self.machine.hdd.toZip(self.shell.getContext(sudo)).then((path)=>{
                                        var id = crypto.randomBytes(16).toString('hex');
                                        self.downloads.set(id, path);
                                        self.sendJSON({op:"DOWNLOAD", uuid: self.uuid, id: id});
                                        self.sendResult();
                                    }, err=>{
                                        self.sendResult();
                                        throw err;
                                    });
                                break;
                                case 'nano':
                                    var path = data.split(' ').slice(1).join(' ');
                                    if(!fp.isAbsolute(path)) path = fp.normalize(self.shell.PWD+'/'+path).replace(/\\/g, '/');
                                    var parent = path.split('/').slice(0, path.split('/').length - 1);
                                    if(self.machine.hdd.hasPermission(self.shell.getContext(sudo), path, 'write') || self.machine.hdd.hasPermission(self.getContext(sudo), parent, 'write'))
                                    {
                                        if(!self.machine.hdd.vol.existsSync(path)) self.machine.hdd.write_file(self.shell.getContext(sudo), path, `function(context, args){\n\n}`);
                                        var cur = self.machine.hdd.read_file(self.shell.getContext(sudo), path);
                                        if(cur.ok)
                                        {
                                            cur.data = (cur.data != null ? cur.data : '');
                                            // Mask user passwords
                                            if(path == '/etc/passwd')
                                            {
                                                self.sendResult({ok:false, msg: '`DError`: Access denied'});
                                                return;
                                            }
                                            var fid = self.open(path);
                                            self.sendJSON({op:"TEXTEDIT", path: path, fid: fid, uuid: self.uuid});
                                            self.sendResult(`Opening ${path}`);
                                        }
                                        else
                                        {
                                            self.sendResult(cur);
                                        }
                                    }
                                    else
                                    {
                                        self.sendResult({ok:false, msg:"`DError`: Access denied"});
                                    }
                                break;
                                case 'ssh':
                                    if(!self.remote)
                                    {
                                        var dest = data.split(' ')[1];
                                        if((new RegExp(/\d.\d.\d.\d/)).test(dest))
                                        {
                                            var machine = storage.gateways.get(self.machine.nic.gateway).resolve(dest);
                                            if(machine != null)
                                            {
                                                self.shell.disconnect();
                                                self.shell = null;
                                                self.shell = machine.shell(self.uuid, self, self.machine.nic.address);
                                                if(self.shell == null)
                                                {
                                                    self.sendResult({ok: false, msg:"`DError`: Connection refused."});
                                                    self.machine = storage.machines.get(this.home);
                                                    self.shell = self.machine.shell(self.uuid, self);
                                                    self.shell.login();
                                                    return;
                                                }
                                                self.machine = machine;
                                                self.remote = true;
                                                self.sendResult();
                                                self.shell.login(self.machine.nic.address);
                                            }
                                            else
                                            {
                                                self.sendResult({ok:false, msg:"`DError`: Host not found"});
                                            }
                                        }
                                        else
                                        {
                                            self.sendResult({ok:false, msg:"`DError`: Invalid destination"});
                                        }
                                    }
                                break; 
                                case 'disconnect':
                                    if(this.remote)
                                    {
                                        self.shell.disconnect();
                                        self.shell = null;
                                        self.machine = storage.machines.get(this.home);
                                        self.shell = self.machine.shell(self.uuid, self);
                                        this.remote = false;
                                        self.sendResult({ok:true, msg:"`2Disconnected`"});
                                        self.shell.login();
                                    }
                                break;
                                case 'logout':
                                    self.sendResult('Logged out.');
                                    self.shell = null;
                                    self.shell = self.machine.shell(self.uuid, self);
                                    self.shell.login();
                                break;
                                default:
                                    var ret = self.shell.command(dobj.command);
                                    self.sendResult(ret);
                                break;
                            }
                        break;
                    }
                }
            } catch(e){
                console.log(e);
            }
        }
    }
    sendJSON(obj){
        obj = Object.assign({}, obj, {t: Date.now()});
        this.socket.send(JSON.stringify(obj));
    }
    sendRetval(data, success = true){
        let obj = {
            success: success,
            data:{
                retval: data
            }
        };
        this.sendJSON(obj);
    }
    sendResult(data, success = true){
        this.sendJSON({op:"RUN"});
        this.sendRetval(data, success);
    }
}
var server = config.ssl ? https.createServer({key: fs.readFileSync('/opt/keys/mudjs.key'), cert: fs.readFileSync('/opt/keys/mudjs.crt')}) : http.createServer();
var port = config.port;
var ws = new WebSocket.Server({server: server});
ws.on('connection', (socket)=>{
    console.log('Client connected.');
    socket.on('pong', ()=>{
        socket.isAlive = true;
    });
    var nsock = new WSocket(socket);
    clients.set(nsock.uuid, nsock);
});
setInterval(()=>{
    ws.clients.forEach(socket=>{
        if(socket.isAlive === false) return socket.terminate();
        socket.isAlive = false;
        socket.ping(()=>{});
    });
}, 30000);
server.listen(port, ()=>{
    console.log(`Listening on port ${port}`);
    if(config.updates)
    {
        console.log('Starting updater');
        if(!fs.existsSync(process.cwd()+'/update.check'))
        {
            fs.writeFileSync(process.cwd()+'/update.check', JSON.stringify({ts: Date.now()+15000}));
        }

        var ts = JSON.parse(fs.readFileSync(process.cwd()+'/update.check').toString()).ts;

        setInterval(()=>{
            if(Date.now() >= ts)
            {
                ts = Date.now()+15000;
                fs.writeFileSync(process.cwd()+'/update.check', JSON.stringify({ts: ts}));
                console.log('Checking for update...');
                exec('git fetch', (err, stdout, stderr)=>{
                    if(stderr.length > 0)
                    {
                        console.log(err, stdout, stderr);
                        console.log('Update found. Applying...')
                        clients.forEach(socket=>{
                            socket.sendRetval("System: `2A server update was found and is being applied.`");
                        });
                        exec('git pull', ()=>{
                            exec('npm install', ()=>{
                                clients.forEach(socket=>{
                                    socket.sendRetval("System: `2The server is restarting to apply an update.`");
                                });
                                setTimeout(()=>{
                                    process.exit(0);
                                }, 2500);
                            });
                        });
                    }
                    else console.log('No update found.');
                });
            }
        }, 1)
    }
    else console.log("Update system disabled.");
});

const api_error = {
    "CUSTOM": async function(code, message, ctx){
        ctx.status = code;
        ctx.body = {ts: Date.now(), success: false, error: message};
    },
    "BODY_MISSING": async function(ctx){
        ctx.status = 400;
        ctx.body = {ts: Date.now(), success: false, error: 'Request body missing or incomplete.'};
    },
    "CLIENT_MISSING": async function(ctx){
        ctx.status = 401;
        ctx.body = {ts: Date.now(), success: false, error: 'Invalid UUID provided.'};
    },
    "PATH_INVALID": async function(ctx){
        ctx.status = 400;
        ctx.body = {ts: Date.now(), success: false, error: 'Invalid path provided.'};
    },
    "UNAUTHORIZED": async function(ctx){
        ctx.status = 403;
        ctx.body = {ts: Date.now(), success: false, error: 'Forbidden.'};
    },
    "AUTH_MISSING": async function(ctx){
        ctx.status = 401;
        ctx.body = {ts: Date.now(), success: false, error: 'No authentication provided.'};
    },
    "AUTH_DUPE": async function(ctx){
        ctx.status = 400;
        ctx.body = {ts: Date.now(), success: false, error: 'Already authenticated.'};
    }
}

api_v1.post('/auth', async ctx=>{
    if(ctx.session.authorized === true)
    {
        await api_error.AUTH_DUPE(ctx);
    }
    else
    {
        if(ctx.request.body.access_token != undefined)
        {
            if(ctx.session.nonce != ctx.request.body.nonce)
            {
                await api_error.UNAUTHORIZED(ctx);
            }
            else
            {
                ctx.session.access_token = ctx.request.body.access_token;
                ctx.body = {redirect: true};
            }
        }
        else
        {
            await api_error.UNAUTHORIZED(ctx);
        }
    }
});

api_v1.post('/filesystem/upload', async ctx=>{
    var dobj = ctx.request.body;
    if(!dobj || Object.keys(dobj).length == 0) await api_error.BODY_MISSING(ctx);
    else
    {
        if(!dobj.uuid || !dobj.fid || !dobj.path || !dobj.data) await api_error.BODY_MISSING(ctx);
        else
        {
            var client = clients.get(dobj.uuid);
            if(!client) await api_error.CLIENT_MISSING(ctx);
            else
            {
                if(!client.openfiles.get(dobj.fid)) await api_error.UNAUTHORIZED(ctx);
                else
                {
                    client.upload(dobj.fid, dobj.path, dobj.data);
                    ctx.body = {ts: Date.now(), success: true};
                }
            }
        }
    }
});

api_v1.post('/filesystem/close', async ctx=>{
    var dobj = ctx.request.body;
    if(!dobj || Object.keys(dobj).length == 0 || !dobj.uuid || !dobj.fid || !dobj.path) await api_error.BODY_MISSING(ctx);
    else
    {
        var client = clients.get(dobj.uuid);
        if(!client) await api_error.CLIENT_MISSING(ctx);
        else
        {
            if(client.openfiles.get(dobj.fid))
            {
                client.close(dobj.fid, dobj.path);
                ctx.body = {ts: Date.now(), success:true};
            }
            else
            {
                await api_error.CUSTOM(400, 'Invalid file', ctx);
            }
        }
    }
});

api_v1.get('/oauth', async ctx=>{
    if(ctx.session.nonce == undefined)
    {
        ctx.redirect('/');
    }
    else if(ctx.query.access_token && ctx.query.n)
    {
        if(ctx.query.n == ctx.session.nonce)
        {
            ctx.session.authorized = true;
            ctx.session.access_token = ctx.query.access_token;
            ctx.redirect('/terminal');
        }
    }
    else
    {
        ctx.body = fs.readFileSync(process.cwd()+'/oauth/index.html').toString();
    }
});

const apiVersion = 0;

api_v1.use(async (ctx, next)=>{
    if(ctx.query.token || ctx.headers['authorization'] || ctx.path == '/api/v1/oauth') await next();
    else await api_error.UNAUTHORIZED(ctx);
});

api_v1.use(async (ctx, next)=>{
    if(ctx.path == '/api/v1/oauth') await next();
    else
    {
        var token = (ctx.query.token ? ctx.query.token : ctx.headers['authorization']);
        var user = Array.from(storage.users).map(el=>{
            return el[1];
        }).find(el=>{
            return el.api.token == token;
        });
        if(!user) await api_error.UNAUTHORIZED(ctx);
        else
        {
            ctx.caller = {uid: user.uid, home: user.home};
            await next();
        }
    }
});

api_v1.get('/', async ctx=>{
    ctx.body = {ts: Date.now(), version: apiVersion, caller: ctx.caller.uid};
});

api_v1.get('/chat/:channel', async ctx=>{
    var channel = storage.chat.channels.get(ctx.params.channel);
    if(!channel) ctx.body = {ts: Date.now(), success: false, error: "Channel not found"};
    else
    {
        if(!channel.users.find(el=>{
            return el == ctx.caller.uid;
        }))
        {
            ctx.body = {ts: Date.now(), success: false, error: "User not in this channel"};
        }
        else
        {
            ctx.body = {
                ts: Date.now(),
                success: true,
                channel: {
                    notifications: channel.api.chat
                }
            };
        }
    }
});

router.use('/v1', api_v1.routes());
var root = new Router();
root.get('/terminal', async (ctx)=>{
    if((!ctx.session.nonce || !ctx.session.access_token) && config.live)
    {
        ctx.session.nonce = crypto.randomBytes(16).toString('hex')+(Buffer.from([Date.now()]).toString('hex'));
        var redirect = encodeURIComponent('https://vrealm.mudjs.net/api/v1/oauth');
        if(ctx.userAgent.isBot) redirect = encodeURIComponent('urn:ietf:wg:oauth:2.0:oob');
        ctx.redirect(`https://itch.io/user/oauth?client_id=34219431406c4fe72e88d011d85b62ef&scope=profile%3Ame&response_type=token&redirect_uri=${redirect}&state=${ctx.session.nonce}`);
    }
    else if(!config.live)
    {
        ctx.session.access_token = 'DEV';
        ctx.body = fs.readFileSync(process.cwd()+'/www/terminal/index.html').toString().replace('false//#access_token;', `${ctx.session.access_token}`);
    }
    else
    {
        ctx.body = fs.readFileSync(process.cwd()+'/www/terminal/index.html').toString().replace('false//#access_token;', `${ctx.session.access_token}`);
    }
});
root.get('/editor', async ctx=>{
    if(!ctx.query.uuid || !ctx.query.fid)
    {
        ctx.body = '';
        return;
    }
    var client = clients.get(ctx.query.uuid);
    if(!client)
    {
        ctx.body = '';
        return;
    }
    if(!client.openfiles.get(ctx.query.fid))
    {
        ctx.body = '';
        return;
    }
    var data = client.machine.hdd.vol.readFileSync(client.openfiles.get(ctx.query.fid), 'utf8');
    var page = fs.readFileSync(process.cwd()+'/www/editor/index.html').toString().replace(`''//#editor-data`, `${data}`);
    ctx.body = page;
});
root.get('/download/:uuid/:id', async ctx=>{
    console.log(ctx.params);
    if(!ctx.params.uuid || !ctx.params.id) ctx.body = '';
    else
    {
        var client = clients.get(ctx.params.uuid);
        if(!client) ctx.body = '';
        else
        {
            var path = client.downloads.get(ctx.params.id);
            console.log(path);
            if(!path) ctx.body = '';
            else
            {
                await send(ctx, path);
                client.downloads.delete(ctx.params.id);
                fs.unlinkSync(path);
            }
        }
    }
});
app.use(root.routes());
app.use(serve(process.cwd()+'/www'));
app.use(mount('/api', router.routes()));
app.listen(3005, ()=>{
    console.log('HTTP server online.');
});

setInterval(()=>{
    clients.forEach(client=>{
        var user = storage.users.get(client.uid);
        if(!user) return;
        if(user.notifications.length > 0)
        {
            var notifications = user.notifications.slice(0);
            user.notifications = [];
            notifications.forEach(notif=>{
                switch(notif.type){
                    case 'chat':
                        function createMessage(user, channel, msg, ts){
                            let fin = inet.html.encode(`[\`C${ts}\`::\`V${channel}\`][${user}]\`C:\` ${msg}`);
                            return fin;
                        }
                        var msg = createMessage(notif.data.from, notif.data.channel, notif.data.content, new Date(notif.ts).toTimeStamp());
                        client.sendRetval(msg);
                    break;
                    case 'notification':
                        client.sendRetval(notif.data);
                    break;
                }
            });
        }
    });
}, 1000);