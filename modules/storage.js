module.exports.machines = new Map();
module.exports.gateways = new Map();
module.exports.dns = null;
module.exports.users = new Map();
module.exports.item_template = new Map();

setInterval(()=>{
    module.exports.machines.forEach(machine=>{
        machine.tick();
    });
}, 1);