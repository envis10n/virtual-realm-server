const crypto = require('crypto');
Math.rand_int = function(max = 1, min = 0){
    return Math.round(Math.random() * (max - min) + min);
}
Map.fromJSON = function(obj){
    let temp = new Map();
    if(!obj) return temp;
    Object.keys(obj).forEach(key=>{
        temp.set(key, obj[key]);
    });
    return temp;
}
Map.prototype.toJSON = function(){
    var obj = {};
    let arr = Array.from(this);
    arr.forEach(el=>{
        obj[el[0]] = el[1];
    });
    return obj;
}
Date.prototype.toTimeStamp = function(){
    let hours = this.getHours();
    let min = this.getMinutes();
    return (hours >= 10 ? ''+hours : '0'+hours)+(min >= 10 ? ''+min : '0'+min);
}
const Entities = require('html-entities').AllHtmlEntities;
const html = new Entities();
module.exports.html = html;

class DNS extends Map{
    constructor(id){
        super();
        this.id = id;
    }
    resolve(hostname){
        let address = this.get(hostname);
        return address != undefined ? address : false;
    }
    toJObj(){
        var obj = {
            id: this.id,
            data: {}
        };
        let arr = Array.from(this);
        arr.forEach(el=>{
            obj.data[el[0]] = el[1];
        });
        return obj;
    }
    static fromSave(obj){
        let temp = new DNS(obj.id);
        Object.keys(obj.data).forEach(key=>{
            temp.set(key, obj.data[key]);
        });
        return temp;
    }
}

class DHCP{
    constructor(block){
        this.block = block;
        this.mapped = new Map();
        this.assigned = new Map();
        this.expires = new Map();
    }
    set(key, value){
        return this.mapped.set(key, value);
    }
    get(key){
        return this.mapped.get(key);
    }
    delete(key){
        return this.mapped.delete(key);
    }
    assign(hwid){
        var address = this.block.split('.');
        while(true){
            address[2] = Math.rand_int(255, 1);
            address[3] = Math.rand_int(255, 1);
            address = address.join('.');
            if(!this.mapped.get(address)) break;
        }
        this.mapped.set(address, hwid);
        this.assigned.set(hwid, address);
        this.expires.set(address, Date.now()+(1000*60*60*24*7));
        return address;
    }
    release(address){
        var hwid = this.get(address);
        this.delete(address);
        this.assigned.delete(hwid);
        this.expires.delete(address);
    }
    renew(address){
        let hwid = this.get(address);
        this.delete(address);
        return this.assign(hwid);
    }
    retrieve(hwid){
        return this.assigned.get(hwid);
    }
    toJObj(){
        return {
            block: this.block,
            mapped: this.mapped.toJSON(),
            assigned: this.assigned.toJSON(),
            expires: this.expires.toJSON()
        }
    }
}

module.exports.DNS = DNS;
module.exports.DHCP = DHCP;
module.exports.generateHWID = function(){
    return crypto.randomBytes(8).toString('hex')+'-'+crypto.randomBytes(16).toString('hex')+'-'+crypto.randomBytes(4).toString('hex');
}