const MongoClient = require('mongodb').MongoClient;
const EventEmitter = require('events');

var client = null;

module.exports = new EventEmitter();

MongoClient.connect('mongodb://localhost:27017/vrealm_t', { useNewUrlParser: true }, (err, cli)=>{
    if(err) throw err;
    client = cli;
    module.exports.emit('ready');
});

module.exports.con = function(collection){
    if(client == null) return null;
    return (collection != undefined ? client.db().collection(collection) : client.db());
}

module.exports.store = {
    async get (key, maxAge, {rolling}) {
        var sess = await client.db().collection('sessions').findOne({key: key});
        if(!sess) return null;
        if(Date.now() >= sess.expire)
        {
            return null;
        }
        if(rolling)
        {
            sess.expire = Date.now()+maxAge;
            await client.db().collection('sessions').updateOne({key: key},{$set:sess});
        }
        return sess.session;
    },
    async set (key, sess, maxAge, {rolling, changed}){
        var ses = await client.db().collection('sessions').findOne({key: key});
        if(!ses)
        {
            await client.db().collection('sessions').insertOne({expire: Date.now()+maxAge, key: key, session: sess});
        }
        else
        {
            if(rolling)
            {
                ses.expire = Date.now()+maxAge;
            }
            ses.session = sess;
            await client.db().collection('sessions').updateOne({key: key},{$set:ses});
        }
    },
    async destroy (key){
        await client.db().collection('sessions').deleteOne({key: key});
    }
}